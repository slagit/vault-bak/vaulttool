package run

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	"gitlab.com/slagit/vault/vaulttool/internal/transport"
	"gitlab.com/slagit/vault/vaulttool/internal/vault"
	"gitlab.com/slagit/vault/vaulttool/manager"
)

type Module interface {
	Run(context.Context, []string, string) error
}

type module struct {
	joinAddress string
	transport   transport.Configuration
}

func (m *module) Run(ctx context.Context, args []string, tokenRole string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	if tokenRole != "" {
		s, err := client.Auth().Token().CreateWithRole(nil, tokenRole)
		if err != nil {
			return err
		}
		client.SetToken(s.Auth.ClientToken)
		defer func() {
			if err := client.Auth().Token().RevokeSelf(""); err != nil {
				fmt.Fprintf(os.Stderr, "WARNING: Error revoking role token: %s\n", err)
			}
		}()
	}

	c := exec.CommandContext(ctx, args[0], args[1:]...)
	c.Env = append(os.Environ(), "VAULT_ADDR="+m.joinAddress, "VAULT_TOKEN="+client.Token())
	if m.transport.CACertificateData != "" {
		cf, err := ioutil.TempFile("", "vaulttool.*")
		if err != nil {
			return err
		}
		defer os.Remove(cf.Name())
		if _, err := cf.Write([]byte(m.transport.CACertificateData)); err != nil {
			return err
		}
		if err := cf.Close(); err != nil {
			return err
		}
		c.Env = append(c.Env, "VAULT_CACERT="+cf.Name())
	} else if m.transport.CACertificate != "" {
		c.Env = append(c.Env, "VAULT_CACERT="+m.transport.CACertificate)
	}
	c.Stdin = os.Stdin
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	return c.Run()
}

type Configuration struct {
	Vault manager.VaultConfiguration
}

func New(cfg *Configuration) (Module, error) {
	return &module{
		joinAddress: cfg.Vault.JoinAddress,
		transport:   cfg.Vault.Transport,
	}, nil
}
