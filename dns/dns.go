package dns

import (
	"context"
	"fmt"
)

type Configuration struct {
	Type         string
	DigitalOcean *DigitalOceanConfiguration `yaml:"digital-ocean"`
	HostsFile    *HostsFileConfiguration    `yaml:"hosts-file"`
}

type Module interface {
	Create(context.Context, string, string, string) (string, error)
	Delete(context.Context, string) error
	DeleteAll(context.Context, string, string, string) error
}

func New(config *Configuration) (Module, error) {
	switch config.Type {
	case "DigitalOcean":
		return newDigitalOcean(config.DigitalOcean)
	case "HostsFile":
		return newHostsFile(config.HostsFile)
	default:
		return nil, fmt.Errorf("Unknown DNS type: %s", config.Type)
	}
}
