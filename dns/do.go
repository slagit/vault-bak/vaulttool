package dns

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/digitalocean/godo"
	"gitlab.com/slagit/vault/vaulttool/internal/do"
)

type DigitalOceanConfiguration struct {
	do.Configuration `yaml:",inline"`
}

type DigitalOceanModule struct {
	client *godo.Client
}

func (m *DigitalOceanModule) splitFQDN(ctx context.Context, fqdn string) (string, string, error) {
	log.Printf("Splitting %s into hostname and domainname", fqdn)

	var domain *godo.Domain
	for l := new(do.Lister); !l.Done; {
		domains, resp, err := m.client.Domains.List(ctx, &l.ListOptions)
		if err != nil {
			return "", "", err
		}

		for i := range domains {
			if domains[i].Name == fqdn {
				return "@", domains[i].Name, nil
			}
			if strings.HasSuffix(fqdn, "."+domains[i].Name) {
				if domain == nil || len(domains[i].Name) > len(domain.Name) {
					domain = &domains[i]
				}
			}
		}

		err = l.Update(resp)
		if err != nil {
			return "", "", err
		}
	}

	if domain == nil {
		return "", "", fmt.Errorf("No suitable domain found for %s", fqdn)
	}

	return fqdn[:len(fqdn)-len(domain.Name)-1], domain.Name, nil
}

func (m *DigitalOceanModule) Create(ctx context.Context, fqdn, ofType, data string) (string, error) {
	hostname, domainname, err := m.splitFQDN(ctx, fqdn)
	if err != nil {
		return "", err
	}
	log.Printf("Creating DNS record %s in domain %s...", hostname, domainname)
	record, _, err := m.client.Domains.CreateRecord(ctx, domainname, &godo.DomainRecordEditRequest{
		Type: ofType,
		Name: hostname,
		Data: data,
		TTL:  30,
	})
	if err != nil {
		return "", err
	}

	log.Println("Sleeping 5 seconds for DNS propagation...")
	time.Sleep(5 * time.Second)

	return fmt.Sprintf("%d.%s", record.ID, domainname), nil
}

func (m *DigitalOceanModule) Delete(ctx context.Context, id string) error {
	idParts := strings.SplitN(id, ".", 2)
	if len(idParts) != 2 {
		// TODO ERROR
	}
	recordId, err := strconv.Atoi(idParts[0])
	if err != nil {
		return err
	}
	domainname := idParts[1]
	if err != nil {
		return err
	}
	log.Printf("Deleting DNS record id %d in domain %s...", recordId, domainname)
	_, err = m.client.Domains.DeleteRecord(ctx, domainname, recordId)
	return err
}

func (m *DigitalOceanModule) DeleteAll(ctx context.Context, fqdn, ofType, data string) error {
	hostname, domainname, err := m.splitFQDN(ctx, fqdn)
	if err != nil {
		return err
	}
	log.Printf("Looking for DNS record %s in domain %s...", hostname, domainname)
	for l := new(do.Lister); !l.Done; {
		records, resp, err := m.client.Domains.RecordsByTypeAndName(ctx, domainname, ofType, fqdn, &l.ListOptions)
		if err != nil {
			return err
		}

		for i := range records {
			if records[i].Data == data {
				log.Printf("Deleting DNS record %s in domain %s...", hostname, domainname)
				_, err = m.client.Domains.DeleteRecord(ctx, domainname, records[i].ID)
				if err != nil {
					return err
				}
			}
		}

		err = l.Update(resp)
		if err != nil {
			return err
		}
	}
	return nil
}

func newDigitalOcean(cfg *DigitalOceanConfiguration) (Module, error) {
	if cfg == nil {
		cfg = &DigitalOceanConfiguration{}
	}

	c, err := do.New(&cfg.Configuration)
	if err != nil {
		return nil, err
	}

	return &DigitalOceanModule{
		client: c,
	}, nil
}
