package dns

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type HostsFileConfiguration struct {
}

type HostsFileModule struct {
}

type hostFile map[string]map[string]bool

func loadHostFile(r io.Reader) (hostFile, error) {
	retval := make(hostFile)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		uncomment := strings.SplitN(scanner.Text(), "#", 2)
		parts := strings.Fields(uncomment[0])
		if len(parts) > 0 {
			for _, n := range parts[1:] {
				entry := retval[parts[0]]
				if entry == nil {
					entry = make(map[string]bool)
				}
				entry[n] = true
				retval[parts[0]] = entry
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return retval, nil
}

func loadSystemHostFile() (hostFile, error) {
	f, err := os.Open("/etc/hosts")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return loadHostFile(f)
}

func (hf hostFile) Add(ip, fqdn string) {
	entry := hf[ip]
	if entry == nil {
		entry = make(map[string]bool)
	}
	entry[fqdn] = true
	hf[ip] = entry
}

func (hf hostFile) Remove(ip, fqdn string) {
	delete(hf[ip], fqdn)
}

func (hf hostFile) Write(w io.Writer) error {
	for ip, names := range hf {
		ns := make([]string, 0, len(names))
		for n, ok := range names {
			if ok {
				ns = append(ns, n)
			}
		}

		if len(ns) > 0 {
			_, err := fmt.Fprintf(w, "%s\t%s\n", ip, strings.Join(ns, " "))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (hf hostFile) WriteSystem() error {
	f, err := os.Create("/etc/hosts")
	if err != nil {
		return err
	}

	err = hf.Write(f)
	if err != nil {
		return err
	}

	return f.Close()
}

func (m *HostsFileModule) Create(ctx context.Context, fqdn, ofType, data string) (string, error) {
	if ofType != "A" {
		return "", fmt.Errorf("Unsupported record type: %s", ofType)
	}

	hf, err := loadSystemHostFile()
	if err != nil {
		return "", err
	}

	log.Printf("Adding %s %s to /etc/hosts", data, fqdn)
	hf.Add(data, fqdn)

	err = hf.WriteSystem()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s:%s", data, fqdn), nil
}

func (m *HostsFileModule) Delete(ctx context.Context, id string) error {
	parts := strings.Split(id, ":")
	if len(parts) != 2 {
		return fmt.Errorf("Invalid record id: %s", id)
	}

	hf, err := loadSystemHostFile()
	if err != nil {
		return err
	}

	log.Printf("Removing %s %s from /etc/hosts", parts[0], parts[1])
	hf.Remove(parts[0], parts[1])

	err = hf.WriteSystem()
	if err != nil {
		return err
	}

	return nil
}

func (m *HostsFileModule) DeleteAll(ctx context.Context, fqdn, ofType, data string) error {
	if ofType != "A" {
		return nil
	}

	hf, err := loadSystemHostFile()
	if err != nil {
		return err
	}

	log.Printf("Removing %s %s from /etc/hosts", data, fqdn)
	delete(hf[data], fqdn)

	err = hf.WriteSystem()
	if err != nil {
		return err
	}

	return nil
}

func newHostsFile(cfg *HostsFileConfiguration) (Module, error) {
	if cfg == nil {
		cfg = &HostsFileConfiguration{}
	}

	return &HostsFileModule{}, nil
}
