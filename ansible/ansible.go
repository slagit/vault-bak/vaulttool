package ansible

import (
	"context"
	"errors"
	"fmt"
	"path"

	"github.com/hashicorp/vault/api"
	"gitlab.com/slagit/vault/vaulttool/internal/transport"
	"gitlab.com/slagit/vault/vaulttool/internal/vault"
	"gitlab.com/slagit/vault/vaulttool/manager"
)

type AnsibleConfiguration struct {
	MountPath          string `yaml:"mount-path"`
	RootPasswordLength int    `yaml:"root-password-length"`
	UserPasswordLength int    `yaml:"user-password-length"`
}

type Configuration struct {
	Ansible AnsibleConfiguration
	Vault   manager.VaultConfiguration
}

type Module interface {
	CompleteHostnames(context.Context) []string
	GetPassword(context.Context, string) error
	GetRootPassword(context.Context, string) error
	ListHosts(context.Context) error
	ListKeys(context.Context, string) error
	SetPassword(context.Context, string, string) error
	SetRootPassword(context.Context, string) error
	SetValue(context.Context, string, PathType, string, string) error
}

type module struct {
	joinAddress        string
	mountPath          string
	rootPasswordLength int
	transport          transport.Configuration
	userPasswordLength int
}

func (m *module) GetPassword(ctx context.Context, hostname string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	path, err := m.getPath(client, hostname, PATH_SELF)
	if err != nil {
		return err
	}

	if err := GetPassword(client, &GetPasswordOpts{
		PlainVaultPath: path,
		PlainVaultKey:  "ansible_become_password",
	}); err != nil {
		return err
	}
	return nil
}

func (m *module) GetRootPassword(ctx context.Context, hostname string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	path, err := m.getPath(client, hostname, PATH_RECOVERY)
	if err != nil {
		return err
	}

	if err := GetPassword(client, &GetPasswordOpts{
		PlainVaultPath: path,
		PlainVaultKey:  "root_password",
	}); err != nil {
		return err
	}
	return nil
}

func (m *module) ListHosts(ctx context.Context) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	vaultID, err := getVaultID(client)
	if err != nil {
		return err
	}

	fmt.Println("Shared Hosts:")
	if err := ListHosts(client, path.Join(m.mountPath, "host_vars")); err != nil {
		return err
	}
	fmt.Println("")

	fmt.Println("Identity Hosts:")
	if err = ListHosts(client, path.Join(m.mountPath, "u", vaultID, "host_vars")); err != nil {
		return err
	}
	fmt.Println("")

	fmt.Println("Recovery Hosts:")
	if err = ListHosts(client, path.Join(m.mountPath, "recovery")); err != nil {
		fmt.Printf("%T\n", err)
		return err
	}
	return nil
}

func (m *module) ListKeys(ctx context.Context, hostname string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	vaultID, err := getVaultID(client)
	if err != nil {
		return err
	}

	if err := ListKeys(client, path.Join(m.mountPath, "host_vars", hostname), 1); err != nil {
		return err
	}
	if err := ListKeys(client, path.Join(m.mountPath, "u", vaultID, "host_vars", hostname), 0); err != nil {
		return err
	}
	if err := ListKeys(client, path.Join(m.mountPath, "recovery", hostname), 0); err != nil {
		return err
	}
	return nil
}

func (m *module) SetPassword(ctx context.Context, hostname, username string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	digestPath, err := m.getPath(client, hostname, PATH_SHARED_SELF)
	if err != nil {
		return err
	}
	plainPath, err := m.getPath(client, hostname, PATH_SELF)
	if err != nil {
		return err
	}
	if err := SetPassword(client, &SetPasswordOpts{
		DigestVaultPath: digestPath,
		DigestVaultKey:  fmt.Sprintf("user_%s_password", username),
		PlainVaultPath:  plainPath,
		PlainVaultKey:   "ansible_become_password",
		Length:          m.userPasswordLength,
	}); err != nil {
		return err
	}
	return nil
}

func (m *module) SetRootPassword(ctx context.Context, hostname string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	digestPath, err := m.getPath(client, hostname, PATH_SHARED)
	if err != nil {
		return err
	}
	plainPath, err := m.getPath(client, hostname, PATH_RECOVERY)
	if err != nil {
		return err
	}
	if err := SetPassword(client, &SetPasswordOpts{
		DigestVaultPath: digestPath,
		DigestVaultKey:  fmt.Sprintf("user_root_password"),
		PlainVaultPath:  plainPath,
		PlainVaultKey:   "root_password",
		Length:          m.rootPasswordLength,
	}); err != nil {
		return err
	}
	return nil
}

func (m *module) SetValue(ctx context.Context, hostname string, pType PathType, key, value string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	path, err := m.getPath(client, hostname, pType)
	if err != nil {
		return err
	}
	if err := SetValue(client, path, map[string]string{
		key: value,
	}); err != nil {
		return err
	}
	fmt.Printf("Value written to %s:%s\n", path, key)
	return nil
}

func New(cfg *Configuration) (Module, error) {
	mountPath := cfg.Ansible.MountPath
	if mountPath == "" {
		mountPath = "ansible"
	}
	rootPasswordLength := cfg.Ansible.RootPasswordLength
	if rootPasswordLength == 0 {
		rootPasswordLength = 64
	}
	userPasswordLength := cfg.Ansible.UserPasswordLength
	if userPasswordLength == 0 {
		userPasswordLength = 64
	}
	return &module{
		joinAddress:        cfg.Vault.JoinAddress,
		mountPath:          mountPath,
		rootPasswordLength: rootPasswordLength,
		transport:          cfg.Vault.Transport,
		userPasswordLength: userPasswordLength,
	}, nil
}

type PathType string

const (
	PATH_SELF        PathType = "self"
	PATH_SHARED      PathType = "shared"
	PATH_SHARED_SELF PathType = "shared-self"
	PATH_RECOVERY    PathType = "recovery"
)

func (m *module) getPath(c *api.Client, h string, t PathType) (string, error) {
	switch t {
	case PATH_SELF:
		vaultID, err := getVaultID(c)
		if err != nil {
			return "", err
		}
		return path.Join(m.mountPath, "u", vaultID, "host_vars", h), nil
	case PATH_SHARED:
		return path.Join(m.mountPath, "host_vars", h), nil
	case PATH_SHARED_SELF:
		vaultID, err := getVaultID(c)
		if err != nil {
			return "", err
		}
		return path.Join(m.mountPath, "host_vars", h, vaultID), nil
	case PATH_RECOVERY:
		return path.Join(m.mountPath, "recovery", h), nil
	}
	return "", errors.New("Invalid path type")
}

func getVaultID(c *api.Client) (string, error) {
	t, err := c.Auth().Token().LookupSelf()
	if err != nil {
		return "", err
	}

	vaultID := t.Data["entity_id"].(string)

	if vaultID == "" {
		return "", NoEntityId
	}

	return vaultID, nil
}
