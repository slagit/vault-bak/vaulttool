package ansible

import (
	"context"
	"path"
	"strings"

	"gitlab.com/slagit/vault/vaulttool/internal/vault"
)

func (m *module) CompleteHostnames(_ context.Context) []string {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return nil
	}

	vaultID, err := getVaultID(client)
	if err != nil {
		return nil
	}

	hostnames := make(map[string]bool)
	for _, p := range []string{
		path.Join(m.mountPath, "host_vars"),
		path.Join(m.mountPath, "u", vaultID, "host_vars"),
		path.Join(m.mountPath, "recovery"),
	} {
		l, err := client.Logical().List(p)
		if err != nil {
			continue
		}
		if l != nil {
			for _, k := range l.Data["keys"].([]interface{}) {
				hostnames[strings.Trim(k.(string), "/")] = true
			}
		}
	}

	retval := make([]string, len(hostnames))
	for h := range hostnames {
		retval = append(retval, h)
	}

	return retval
}
