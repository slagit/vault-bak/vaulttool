package ansible

import (
	"bytes"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strings"

	"github.com/hashicorp/vault/api"
)

const (
	PW_LOWER   = "abcdefghijklmnopqrstuvwxyz"
	PW_UPPER   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	PW_SPECIAL = "!\"#$%&'()*+,-./;:<=>?@[\\]^_`{|}~"
	PW_DIGIT   = "0123456789"
	PW_ALL     = PW_LOWER + PW_UPPER + PW_SPECIAL + PW_DIGIT
)

var (
	NoEntityId = errors.New("Vault token does not have associated entity id")
	NoPassword = errors.New("No password found for vault identity")
)

type GetPasswordOpts struct {
	PlainVaultPath string
	PlainVaultKey  string
}

func GetPassword(c *api.Client, o *GetPasswordOpts) error {
	s, err := c.Logical().Read(o.PlainVaultPath)
	if err != nil {
		return err
	}
	if s == nil {
		return NoPassword
	}

	pw := s.Data[o.PlainVaultKey]
	if pw == "" {
		return NoPassword
	}

	fmt.Println(pw)
	return nil
}

func ListHosts(c *api.Client, p string) error {
	l, err := c.Logical().List(p)
	if err != nil {
		switch et := err.(type) {
		case *api.ResponseError:
			if et.StatusCode == http.StatusForbidden {
				fmt.Println("(Permission Denied)")
				return nil
			}
			return err
		default:
			return err
		}
	}
	hostnames := make(map[string]bool)
	if l != nil {
		for _, k := range l.Data["keys"].([]interface{}) {
			h := strings.Trim(k.(string), "/")
			if !hostnames[h] {
				fmt.Printf("* %s\n", h)
				hostnames[h] = true
			}
		}
	}
	return nil
}

func ListKeys(c *api.Client, p string, depth int) error {
	if err := listKeys(c, p); err != nil {
		return err
	}
	if depth > 0 {
		l, err := c.Logical().List(p)
		if err != nil {
			switch et := err.(type) {
			case *api.ResponseError:
				if et.StatusCode == http.StatusForbidden {
					fmt.Printf("%s: (Permission Denied)\n", p)
					return nil
				}
				return err
			default:
				return err
			}
		}
		if l != nil {
			for _, k := range l.Data["keys"].([]interface{}) {
				if err := ListKeys(c, path.Join(p, k.(string)), depth-1); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

type SetPasswordOpts struct {
	DigestVaultPath string
	DigestVaultKey  string
	PlainVaultPath  string
	PlainVaultKey   string
	Length          int
}

func SetPassword(c *api.Client, o *SetPasswordOpts) error {
	pw, err := generatePassword(rand.Reader, o.Length)
	if err != nil {
		return err
	}

	pwhash, err := hashPassword(pw)
	if err != nil {
		return err
	}
	pwhash = strings.TrimSpace(pwhash)

	if err := SetValue(c, o.DigestVaultPath, map[string]string{
		o.DigestVaultKey: pwhash,
	}); err != nil {
		return err
	}

	if err := SetValue(c, o.PlainVaultPath, map[string]string{
		o.PlainVaultKey: pw,
	}); err != nil {
		return err
	}

	fmt.Printf("Password written to %s:%s\n", o.PlainVaultPath, o.PlainVaultKey)
	fmt.Printf("Hashed password written to %s:%s\n", o.DigestVaultPath, o.DigestVaultKey)

	return nil
}

func SetValue(c *api.Client, p string, data map[string]string) error {
	s, err := c.Logical().Read(p)
	if err != nil {
		return err
	}

	var d map[string]interface{}
	if s == nil {
		d = make(map[string]interface{})
	} else {
		d = s.Data
	}

	for k, v := range data {
		d[k] = v
	}

	if _, err := c.Logical().Write(p, d); err != nil {
		return err
	}

	return nil
}

func generatePassword(r io.Reader, n int) (string, error) {
	var pw strings.Builder
	for i := 0; i < n; i += 1 {
		c, err := rand.Int(r, big.NewInt(int64(len(PW_ALL))))
		if err != nil {
			return "", err
		}
		pw.WriteString(string(PW_ALL[c.Int64()]))
	}
	return pw.String(), nil
}

func hashPassword(pw string) (string, error) {
	cmd := exec.Command("openssl", "passwd", "-6", "-stdin")
	cmd.Stdin = bytes.NewBuffer([]byte(pw))
	var pwHash bytes.Buffer
	cmd.Stdout = &pwHash
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return "", err
	}
	return string(pwHash.Bytes()), nil
}

func listKeys(c *api.Client, p string) error {
	s, err := c.Logical().Read(p)
	if err != nil {
		switch et := err.(type) {
		case *api.ResponseError:
			if et.StatusCode == http.StatusForbidden {
				fmt.Printf("%s:\n", p)
				fmt.Println("(Permission Denied)")
				return nil
			}
			return err
		default:
			return err
		}
	}

	if s != nil {
		fmt.Printf("%s:\n", p)
		for k := range s.Data {
			fmt.Printf("* %s\n", k)
		}
	}

	return nil
}
