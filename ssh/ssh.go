package ssh

import (
	"context"
	"encoding/base64"
	"errors"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"strings"
	"time"

	"github.com/hashicorp/vault/api"
	"gitlab.com/slagit/vault/vaulttool/internal/transport"
	"gitlab.com/slagit/vault/vaulttool/internal/vault"
	"gitlab.com/slagit/vault/vaulttool/manager"
	"golang.org/x/crypto/ssh"
)

var (
	certGrace time.Duration
)

type Module interface {
	Run(context.Context, []string) error
}

type module struct {
	joinAddress string
	transport   transport.Configuration
}

func (m *module) Run(ctx context.Context, args []string) error {
	client, err := vault.NewClientWithToken(&m.transport, m.joinAddress)
	if err != nil {
		return err
	}

	kh, err := writeKnownHosts(client)
	if err != nil {
		return err
	}
	defer os.Remove(kh)

	u, err := user.Current()
	if err != nil {
		return err
	}

	if err := refreshCert(client, u.HomeDir+"/.ssh/id_rsa", u.Username); err != nil {
		return err
	}

	ssh_args := []string{
		"-o", "StrictHostKeyChecking=yes",
		"-o", "UserKnownHostsFile=" + kh,
	}
	for _, a := range args {
		ssh_args = append(ssh_args, a)
	}
	c := exec.CommandContext(ctx, "ssh", ssh_args...)
	c.Stdin = os.Stdin
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	return c.Run()
}

type Configuration struct {
	Vault manager.VaultConfiguration
}

func New(cfg *Configuration) (Module, error) {
	return &module{
		joinAddress: cfg.Vault.JoinAddress,
		transport:   cfg.Vault.Transport,
	}, nil
}

func init() {
	var err error
	certGrace, err = time.ParseDuration("10s")
	if err != nil {
		panic(err)
	}
}

func refreshCert(client *api.Client, path, principal string) error {
	kh, err := writeKnownHosts(client)
	if err != nil {
		return err
	}
	defer os.Remove(kh)

	if ok, err := validateCert(path+"-cert.pub", principal); err != nil {
		return err
	} else if !ok {
		ckp, err := ioutil.ReadFile(path + ".pub")
		if err != nil {
			return err
		}
		ckd, err := client.Logical().Write("ssh-client-signer/sign/"+principal, map[string]interface{}{
			"public_key": string(ckp),
		})
		if err != nil {
			return err
		}
		if err := ioutil.WriteFile(path+"-cert.pub", []byte(ckd.Data["signed_key"].(string)), 0644); err != nil {
			return err
		}
	}
	return nil
}

func validateCert(path, principal string) (bool, error) {
	d, err := ioutil.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	parts := strings.Split(string(d), " ")
	if len(parts) < 2 {
		return false, errors.New("Invalid certificate in " + path)
	}
	raw := make([]byte, base64.StdEncoding.DecodedLen(len(parts[1])))
	n, err := base64.StdEncoding.Decode(raw, []byte(parts[1]))
	if err != nil {
		return false, err
	}
	k, err := ssh.ParsePublicKey(raw[:n])
	if err != nil {
		return false, err
	}
	crt := k.(*ssh.Certificate)
	if crt == nil {
		return false, errors.New("Certificate not found in " + path)
	}
	if time.Now().Add(certGrace).After(time.Unix(int64(crt.ValidBefore), 0)) {
		return false, nil
	}
	for _, p := range crt.ValidPrincipals {
		if p == principal {
			return true, nil
		}
	}
	return false, nil
}

func writeKnownHosts(client *api.Client) (string, error) {
	s, err := client.Logical().Read("ssh-host-signer/config/ca")
	if err != nil {
		return "", err
	}
	if s == nil {
		return "", errors.New("Host certificate not found in vault")
	}
	line := "@cert-authority * " + s.Data["public_key"].(string)

	return writeTemp([]byte(line))
}

func writeTemp(data []byte) (string, error) {
	f, err := ioutil.TempFile("", "vaulttool.*")
	if err != nil {
		return "", err
	}
	if _, err := f.Write(data); err != nil {
		os.Remove(f.Name())
		return "", err
	}
	if err := f.Close(); err != nil {
		os.Remove(f.Name())
		return "", err
	}
	return f.Name(), nil
}
