module gitlab.com/slagit/vault/vaulttool

go 1.15

require (
	github.com/digitalocean/godo v1.58.0
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2
	github.com/hashicorp/vault/api v1.0.4
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	gitlab.com/slagit/vault/config v1.2.14
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	golang.org/x/net v0.0.0-20201029221708-28c70e62bb1d // indirect
	golang.org/x/oauth2 v0.0.0-20210220000619-9bb904979d93
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
