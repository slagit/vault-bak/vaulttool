package manager

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"net"
	"strings"

	vapi "github.com/hashicorp/vault/api"
	"gitlab.com/slagit/vault/vaulttool/cert"
	"gitlab.com/slagit/vault/vaulttool/dns"
	"gitlab.com/slagit/vault/vaulttool/internal/transport"
	"gitlab.com/slagit/vault/vaulttool/internal/vault"
	"gitlab.com/slagit/vault/vaulttool/lb"
	"gitlab.com/slagit/vault/vaulttool/vm"
)

type SealConfiguration struct {
	Address   string                  `yaml:"address"`
	KeyName   string                  `yaml:"key-name"`
	MountPath string                  `yaml:"mount-path"`
	RoleName  string                  `yaml:"role-name"`
	Transport transport.Configuration `yaml:",inline"`
}

type VaultConfiguration struct {
	JoinAddress     string                  `yaml:"join-address"`
	Transport       transport.Configuration `yaml:",inline"`
	SecretThreshold int                     `yaml:"secret-threshold"`
	PGPKeys         PGPKeys                 `yaml:"pgp-keys"`
	RootTokenPGPKey PGPKey                  `yaml:"root-token-pgp-key"`
	Seal            *SealConfiguration
}

type Configuration struct {
	ExtraHostnames []string `yaml:"extra-hostnames"`
	HostnamePrefix string   `yaml:"hostname-prefix"`
	Domain         string   `yaml:"domain"`
	Cert           *cert.Configuration
	DNS            *dns.Configuration
	LB             *lb.Configuration
	Vault          VaultConfiguration
	VM             *vm.Configuration
}

type Module struct {
	hostnamePrefix  string
	domain          string
	cert            cert.Module
	dns             dns.Module
	extraHostnames  []string
	joinAddress     string
	vm              vm.Module
	secretThreshold int
	pgpKeys         []PGPData
	rootTokenPGPKey PGPData
	transport       transport.Configuration
	lb              lb.Module
	seal            *SealConfiguration
}

func (m *Module) createCertificate(ctx context.Context, hostname []string) (string, string, error) {
	key, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return "", "", err
	}

	chain, err := m.cert.Request(ctx, key, hostname...)
	if err != nil {
		return "", "", err
	}

	derKey, err := x509.MarshalPKCS8PrivateKey(key)
	if err != nil {
		return "", "", err
	}

	var pemKey strings.Builder
	err = pem.Encode(&pemKey, &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: derKey,
	})
	if err != nil {
		return "", "", err
	}

	var pemCert strings.Builder
	for i := range chain {
		err = pem.Encode(&pemCert, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: chain[i].Raw,
		})
		if err != nil {
			return "", "", err
		}
	}

	return pemCert.String(), pemKey.String(), nil
}

type InitResponseKey struct {
	ID  string
	Key string
}

type InitResponse struct {
	FQDN      string
	Keys      []InitResponseKey
	RootToken InitResponseKey
}

func (m *Module) Init(ctx context.Context) (*InitResponse, error) {
	hostname, ip, err := m.createVault(ctx)
	if err != nil {
		return nil, err
	}

	client, err := vault.NewClient(&m.transport, fmt.Sprintf("https://%s", hostname))
	if err != nil {
		return nil, err
	}

	if err := vault.Wait(client); err != nil {
		return nil, err
	}

	pgpKeys := make([]string, len(m.pgpKeys))
	for i := range pgpKeys {
		pgpKeys[i] = m.pgpKeys[i].VaultData
	}

	var initRequest vapi.InitRequest
	if m.seal == nil {
		initRequest.SecretShares = len(m.pgpKeys)
		initRequest.SecretThreshold = m.secretThreshold
		initRequest.PGPKeys = pgpKeys
		initRequest.RootTokenPGPKey = m.rootTokenPGPKey.VaultData
	} else {
		initRequest.RecoveryShares = len(m.pgpKeys)
		initRequest.RecoveryThreshold = m.secretThreshold
		initRequest.RecoveryPGPKeys = pgpKeys
		initRequest.RootTokenPGPKey = m.rootTokenPGPKey.VaultData
	}

	response, err := client.Sys().Init(&initRequest)
	if err != nil {
		return nil, err
	}

	if m.lb != nil {
		if err := m.lb.Add(ctx, ip); err != nil {
			return nil, err
		}
	}

	var keys []string
	if m.seal == nil {
		keys = response.KeysB64
	} else {
		keys = response.RecoveryKeysB64
	}

	retval := &InitResponse{
		FQDN: hostname,
		Keys: make([]InitResponseKey, len(keys)),
	}

	for i := range keys {
		retval.Keys[i].ID = m.pgpKeys[i].ID
		retval.Keys[i].Key = keys[i]
	}
	retval.RootToken.ID = m.rootTokenPGPKey.ID
	retval.RootToken.Key = response.RootToken

	return retval, nil
}

type JoinResponse struct {
	FQDN string
}

func (m *Module) Join(ctx context.Context) (*JoinResponse, error) {
	hostname, ip, err := m.createVault(ctx)
	if err != nil {
		return nil, err
	}

	client, err := vault.NewClient(&m.transport, fmt.Sprintf("https://%s", hostname))
	if err != nil {
		return nil, err
	}

	if err := vault.Wait(client); err != nil {
		return nil, err
	}

	certs, err := m.transport.LoadCerts()
	if err != nil {
		return nil, err
	}
	response, err := client.Sys().RaftJoin(&vapi.RaftJoinRequest{
		LeaderAPIAddr: m.joinAddress,
		LeaderCACert:  string(certs),
	})
	if err != nil {
		return nil, err
	}
	if !response.Joined {
		return nil, errors.New("Failed to join raft cluster")
	}

	if m.lb != nil {
		if err := m.lb.Add(ctx, ip); err != nil {
			return nil, err
		}
	}

	return &JoinResponse{
		FQDN: hostname,
	}, nil
}

func (m *Module) Remove(ctx context.Context, name string) error {
	addrs, err := net.LookupHost(name)
	if err != nil {
		return err
	}

	client, err := vault.NewClientWithToken(&m.transport, fmt.Sprintf("https://%s", name))
	if err != nil {
		return err
	}

	if m.lb != nil {
		for i := range addrs {
			if err := m.lb.Remove(ctx, addrs[i]); err != nil {
				return err
			}
		}
	}

	if err != nil {
		return err
	}
	if err := vault.RaftRemovePeer(ctx, client, &vault.RaftRemovePeerRequest{
		ServerID: name,
	}); err != nil {
		return err
	}

	for i := range addrs {
		if err := m.dns.DeleteAll(ctx, name, "A", addrs[i]); err != nil {
			return err
		}
	}

	for i := range addrs {
		if err := m.vm.Delete(ctx, addrs[i]); err != nil {
			return err
		}
	}

	return nil
}

func New(cfg *Configuration) (*Module, error) {
	if cfg == nil {
		cfg = &Configuration{}
	}

	if cfg.HostnamePrefix == "" {
		panic(errors.New("hostname-prefix is required in configuration"))
	}

	if cfg.Domain == "" {
		panic(errors.New("domain is required in configuration"))
	}

	if cfg.Cert == nil {
		panic(errors.New("cert is required in configuration"))
	}

	if cfg.DNS == nil {
		panic(errors.New("dns is required in configuration"))
	}

	if cfg.VM == nil {
		panic(errors.New("vm is required in configuration"))
	}

	var certModule cert.Module
	if cfg.Cert != nil {
		var err error
		certModule, err = cert.New(cfg.Cert)
		if err != nil {
			panic(err)
		}
	}

	var dnsModule dns.Module
	if cfg.DNS != nil {
		var err error
		dnsModule, err = dns.New(cfg.DNS)
		if err != nil {
			panic(err)
		}
	}

	var vmModule vm.Module
	if cfg.VM != nil {
		var err error
		vmModule, err = vm.New(cfg.VM)
		if err != nil {
			panic(err)
		}
	}

	var lbModule lb.Module
	if cfg.LB != nil {
		var err error
		lbModule, err = lb.New(cfg.LB)
		if err != nil {
			panic(err)
		}
	}

	pgpKeys, err := cfg.Vault.PGPKeys.Load()
	if err != nil {
		panic(err)
	}

	rootKey, err := cfg.Vault.RootTokenPGPKey.Load()
	if err != nil {
		panic(err)
	}

	return &Module{
		cert:            certModule,
		dns:             dnsModule,
		domain:          cfg.Domain,
		extraHostnames:  cfg.ExtraHostnames,
		hostnamePrefix:  cfg.HostnamePrefix,
		joinAddress:     cfg.Vault.JoinAddress,
		vm:              vmModule,
		secretThreshold: cfg.Vault.SecretThreshold,
		pgpKeys:         pgpKeys,
		rootTokenPGPKey: *rootKey,
		transport:       cfg.Vault.Transport,
		lb:              lbModule,
		seal:            cfg.Vault.Seal,
	}, nil
}
