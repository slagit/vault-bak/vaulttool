package manager

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/slagit/vault/config"
	"gitlab.com/slagit/vault/vaulttool/internal/vault"
	randutil "gitlab.com/slagit/vault/vaulttool/rand"
)

func (m *Module) createSeal(ctx context.Context) (*config.SealData, error) {
	client, err := vault.NewClientWithToken(&m.seal.Transport, fmt.Sprintf("https://%s", m.seal.Address))
	if err != nil {
		return nil, err
	}

	client.SetWrappingLookupFunc(func(_, _ string) string { return "120" })

	sec, err := client.Logical().Write(fmt.Sprintf("/auth/token/create/%s", m.seal.RoleName), map[string]interface{}{})
	if err != nil {
		return nil, err
	}

	certs, err := m.seal.Transport.LoadCerts()
	if err != nil {
		return nil, err
	}

	return &config.SealData{
		Address:       fmt.Sprintf("https://%s", m.seal.Address),
		CACertificate: string(certs),
		WrappedToken:  sec.WrapInfo.Token,
		KeyName:       m.seal.KeyName,
		MountPath:     m.seal.MountPath,
	}, nil
}

func (m *Module) createVault(ctx context.Context) (string, string, error) {
	var seal *config.SealData
	if m.seal != nil {
		var err error
		seal, err = m.createSeal(ctx)
		if err != nil {
			return "", "", err
		}
	}

	hostnames := make([]string, len(m.extraHostnames)+1)

	hostnames[0] = fmt.Sprintf("%s-%s.%s", m.hostnamePrefix, randutil.String(7), m.domain)
	copy(hostnames[1:], m.extraHostnames)

	chain, key, err := m.createCertificate(ctx, hostnames)

	userData, err := json.Marshal(config.UserData{
		Vault: config.VaultData{
			APIAddr: fmt.Sprintf("https://%s", hostnames[0]),
			Seal:    seal,
			TLS: config.TLSData{
				Certificate: chain,
				PrivateKey:  key,
			},
		},
	})
	if err != nil {
		return "", "", err
	}

	ip, err := m.vm.Create(ctx, hostnames[0], string(userData))
	if err != nil {
		return "", "", err
	}

	_, err = m.dns.Create(ctx, hostnames[0], "A", ip)
	if err != nil {
		return "", "", err
	}

	return hostnames[0], ip, nil
}
