package manager

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"

	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
)

type PGPData struct {
	ID        string
	VaultData string
}

type PGPKey struct {
	Data     *string `yaml:"data,omitempty"`
	Filename *string `yaml:"filename,omitempty"`
}

func (pgp *PGPKey) getBytes() ([]byte, error) {
	if pgp.Data != nil {
		return []byte(*pgp.Data), nil
	}
	if pgp.Filename != nil {
		log.Printf("Loading PGP key from %s...", *pgp.Filename)
		return ioutil.ReadFile(*pgp.Filename)
	}
	return nil, errors.New("Either pgpkey.data or pgpkey.filename is required")
}

func (pgp *PGPKey) Load() (*PGPData, error) {
	data, err := pgp.getBytes()
	if err != nil {
		return nil, err
	}

	buf := bytes.NewBuffer(data)
	for {
		block, err := armor.Decode(buf)
		if err != nil {
			if err == io.EOF {
				return nil, errors.New("No acceptable gpg key found")
			}
			return nil, err
		}

		if block.Type == "PGP PUBLIC KEY BLOCK" {
			keydata, err := ioutil.ReadAll(block.Body)
			if err != nil {
				return nil, err
			}
			entity, err := openpgp.ReadEntity(packet.NewReader(bytes.NewBuffer(keydata)))
			if err != nil {
				return nil, err
			}
			var id string
			for id = range entity.Identities {
				break
			}
			return &PGPData{
				ID:        id,
				VaultData: base64.StdEncoding.EncodeToString(keydata),
			}, nil
		}
	}
}

type PGPKeys []PGPKey

func (pgp PGPKeys) Load() ([]PGPData, error) {
	retval := make([]PGPData, len(pgp))
	for i := range pgp {
		k, err := pgp[i].Load()
		if err != nil {
			return nil, fmt.Errorf("Error loading pgp key %d: %s", i, err)
		}
		retval[i] = *k
	}
	return retval, nil
}
