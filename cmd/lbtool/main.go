package lbtool

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/internal/arguments"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gitlab.com/slagit/vault/vaulttool/lb"
	"gopkg.in/yaml.v2"
)

type Configuration struct {
	LB lb.Configuration
}

var (
	cmd = &cobra.Command{
		Use:   "lb",
		Short: "manage load balancer entries",
		Long:  `Manage load balancer entries`,
	}
	addCmd = &cobra.Command{
		Use:               "add <ip address>",
		Short:             "add a node to a load balancer",
		Long:              `Add a node to a load balancer.`,
		Args:              cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadLB()
			if err := d.Add(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
	removeCmd = &cobra.Command{
		Use:               "remove <ip address>",
		Short:             "remove a node from a load balancer",
		Long:              `Remove a node from a load balancer.`,
		Args:              cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadLB()
			if err := d.Remove(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
)

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func init() {
	cmd.AddCommand(addCmd)
	cmd.AddCommand(removeCmd)
}

func loadLB() lb.Module {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	r, err := lb.New(&c.LB)
	if err != nil {
		panic(err)
	}
	return r
}
