package vmtool

import (
	"context"
	"io/ioutil"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/internal/arguments"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gitlab.com/slagit/vault/vaulttool/vm"
	"gopkg.in/yaml.v2"
)

type Configuration struct {
	VM vm.Configuration
}

var (
	cmd = &cobra.Command{
		Use:   "vm",
		Short: "manage virtual machines",
		Long:  `Manage virtual machines`,
	}
	createCmd = &cobra.Command{
		Use:   "create <name> <filename>",
		Short: "create a new virtual machine",
		Long:  `Create a new virtual machine based on a configuration file.`,
		Args:  cobra.ExactArgs(2),
		ValidArgsFunction: arguments.ArgList(
			nil,
			arguments.Filename,
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadVM()

			r, err := os.Open(args[1])
			if err != nil {
				panic(err)
			}
			userData, err := ioutil.ReadAll(r)
			r.Close()
			if err != nil {
				panic(err)
			}

			ip, err := d.Create(ctx, args[0], string(userData))
			if err != nil {
				panic(err)
			}
			log.Printf("Created IP Address: %s", ip)
		},
	}
	deleteCmd = &cobra.Command{
		Use:               "delete <ip address>",
		Short:             "delete a virtual machine",
		Long:              `Delete a virtual machine based on a configuration file.`,
		Args:              cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadVM()
			if err := d.Delete(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
)

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func init() {
	cmd.AddCommand(createCmd)
	cmd.AddCommand(deleteCmd)
}

func loadVM() vm.Module {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	r, err := vm.New(&c.VM)
	if err != nil {
		panic(err)
	}
	return r
}
