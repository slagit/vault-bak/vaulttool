package sshtool

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gitlab.com/slagit/vault/vaulttool/ssh"
	"gopkg.in/yaml.v2"
)

var (
	cmd = &cobra.Command{
		Use:   "ssh",
		Short: "ssh to a host using credentials from vault",
		Long:  `Run ssh to connect to a host. The ssh host key will be checked against the host CA in vault, and a client certificate will be used to authenticate.`,

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			r := loadSSH()
			if err := r.Run(ctx, args); err != nil {
				panic(err)
			}
		},
	}
)

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func loadSSH() ssh.Module {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &ssh.Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	r, err := ssh.New(c)
	if err != nil {
		panic(err)
	}
	return r
}
