package runtool

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gitlab.com/slagit/vault/vaulttool/run"
	"gopkg.in/yaml.v2"
)

var (
	tokenRole string
	cmd       = &cobra.Command{
		Use:   "run",
		Short: "run command with vault credentials",
		Long:  `Run a command with vault credentials. Optionally obtain a token for a role.`,

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			r := loadRun()
			if err := r.Run(ctx, args, tokenRole); err != nil {
				panic(err)
			}
		},
	}
)

func init() {
	cmd.PersistentFlags().StringVar(&tokenRole, "role", "", "obtain token for role before running")
}

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func loadRun() run.Module {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &run.Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	r, err := run.New(c)
	if err != nil {
		panic(err)
	}
	return r
}
