package vaulttool

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/internal/arguments"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gitlab.com/slagit/vault/vaulttool/manager"
	"gopkg.in/yaml.v2"
)

var (
	outputFormat string
	cmd          = &cobra.Command{
		Use:   "vault",
		Short: "manage a vault cluster",
		Long:  `Manage a vault cluster`,
	}
	initCmd = &cobra.Command{
		Use:               "init",
		Short:             "initialize a new vault cluster",
		Long:              `Initialize a new vault cluster based on a configuration file.`,
		Args:              cobra.NoArgs,
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadManager()
			result, err := d.Init(ctx)
			if err != nil {
				panic(err)
			}
			switch outputFormat {
			case "json":
				o, err := json.Marshal(result)
				if err != nil {
					panic(err)
				}
				fmt.Println(string(o))
			default:
				fmt.Printf("Hostname: %s\n", result.FQDN)
				for i := range result.Keys {
					fmt.Printf("Unseal Key ID #1: %s\n", result.Keys[i].ID)
					fmt.Printf("Unseal Key #1: %s\n", result.Keys[i].Key)
				}
				fmt.Printf("Root Token: %s\n", result.RootToken.Key)
			}
		},
	}
	joinCmd = &cobra.Command{
		Use:               "join",
		Short:             "join a new node to an existing vault cluster",
		Long:              `Join a new node to an existing vault cluster based on a configuration file.`,
		Args:              cobra.NoArgs,
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadManager()
			result, err := d.Join(ctx)
			if err != nil {
				panic(err)
			}
			switch outputFormat {
			case "json":
				o, err := json.Marshal(result)
				if err != nil {
					panic(err)
				}
				fmt.Println(string(o))
			default:
				fmt.Printf("Hostname: %s\n", result.FQDN)
			}
		},
	}
	removeCmd = &cobra.Command{
		Use:               "remove <node name>",
		Short:             "remove a node from an existing vault cluster",
		Long:              `Remove a new node from an existing vault cluster based on a configuration file. Node must not be the last node in the cluster.`,
		Args:              cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadManager()
			if err := d.Remove(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
)

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func init() {
	cmd.PersistentFlags().StringVarP(&outputFormat, "output", "o", "text", "output format [text|json]")
	cmd.AddCommand(initCmd)
	cmd.AddCommand(joinCmd)
	cmd.AddCommand(removeCmd)
}

func loadManager() *manager.Module {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &manager.Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	r, err := manager.New(c)
	if err != nil {
		panic(err)
	}
	return r
}
