package usertool

import (
	"fmt"
	"strings"
	"syscall"

	"github.com/hashicorp/vault/api"
	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/internal/arguments"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gitlab.com/slagit/vault/vaulttool/internal/transport"
	"gitlab.com/slagit/vault/vaulttool/internal/vault"
	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/yaml.v2"
)

type VaultConfiguration struct {
	JoinAddress string                  `yaml:"join-address"`
	Transport   transport.Configuration `yaml:",inline"`
}

type Configuration struct {
	Vault VaultConfiguration
}

type tokenPolicyFlag []string

func (i *tokenPolicyFlag) String() string {
	return strings.Join(*i, " ")
}

func (i *tokenPolicyFlag) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func (i *tokenPolicyFlag) Type() string {
	return "string"
}

var (
	mountPath     string
	tokenPolicies tokenPolicyFlag
	cmd           = &cobra.Command{
		Use:   "user",
		Short: "manage userpass users",
		Long:  `Manage userpass users in a vault cluster`,
	}
	addCmd = &cobra.Command{
		Use:               "add <username>",
		Short:             "add or update a user",
		Long:              `Add or update a vault userpass user`,
		Args:              cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			username := args[0]
			path := fmt.Sprintf("auth/%s/users/%s", mountPath, username)
			client := loadVault()

			password, err := readPassword()
			if err != nil {
				panic(err)
			}

			if _, err := client.Logical().Write(path, map[string]interface{}{
				"password":       password,
				"token_policies": tokenPolicies,
			}); err != nil {
				panic(err)
			}
		},
	}
	deleteCmd = &cobra.Command{
		Use:               "delete <username>",
		Short:             "delete a user",
		Long:              `Delete a vault userpass user.`,
		Args:              cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			username := args[0]
			path := fmt.Sprintf("auth/%s/users/%s", mountPath, username)
			client := loadVault()

			if _, err := client.Logical().Delete(path); err != nil {
				panic(err)
			}
		},
	}
)

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func init() {
	cmd.PersistentFlags().StringVar(&mountPath, "mount-path", "userpass", "mount path for userpass auth method")
	cmd.AddCommand(addCmd)
	addCmd.PersistentFlags().VarP(&tokenPolicies, "token-policy", "p", "list of policies to encode onto generated tokens (required)")
	addCmd.MarkPersistentFlagRequired("token-policy")
	cmd.AddCommand(deleteCmd)
}

func readPassword() (string, error) {
	for {
		fmt.Print("Enter Password: ")
		bytePassword, err := terminal.ReadPassword(syscall.Stdin)
		fmt.Println("")
		if err != nil {
			return "", err
		}

		fmt.Print("Enter Password (again): ")
		bytePassword2, err := terminal.ReadPassword(syscall.Stdin)
		fmt.Println("")
		if err != nil {
			return "", err
		}

		password := string(bytePassword)
		if password == string(bytePassword2) {
			return password, nil
		}

		fmt.Println("Password mismatch")
	}
}

func loadVault() *api.Client {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	client, err := vault.NewClientWithToken(&c.Vault.Transport, c.Vault.JoinAddress)
	if err != nil {
		panic(err)
	}

	return client
}
