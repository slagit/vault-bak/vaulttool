package dnstool

import (
	"context"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/dns"
	"gitlab.com/slagit/vault/vaulttool/internal/arguments"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gopkg.in/yaml.v2"
)

type Configuration struct {
	DNS dns.Configuration
}

var (
	cmd = &cobra.Command{
		Use:   "dns",
		Short: "manage DNS entries",
		Long:  `Manage DNS entries`,
	}
	createCmd = &cobra.Command{
		Use:   "create <fqdn> <type> <data>",
		Short: "create a new DNS entry",
		Long:  `Create a new DNS entry based on a configuration file.`,
		Args:  cobra.ExactArgs(3),
		ValidArgsFunction: arguments.ArgList(
			nil,
			arguments.Choice([]string{"A", "TXT"}),
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadDNS()
			id, err := d.Create(ctx, args[0], args[1], args[2])
			if err != nil {
				panic(err)
			}
			log.Printf("Created record key %s", id)
		},
	}
	deleteAllCmd = &cobra.Command{
		Use:   "delete-all <fqdn> <type> <data>",
		Short: "delete all matching DNS entries",
		Long:  `Delete all matching DNS entries based on a configuration file.`,
		Args:  cobra.ExactArgs(3),
		ValidArgsFunction: arguments.ArgList(
			nil,
			arguments.Choice([]string{"A", "TXT"}),
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadDNS()
			if err := d.DeleteAll(ctx, args[0], args[1], args[2]); err != nil {
				panic(err)
			}
		},
	}
	deleteCmd = &cobra.Command{
		Use:   "delete <id>",
		Short: "delete a DNS entry",
		Long:  `Delete a DNS entry based on a configuration file.`,
		Args:  cobra.ExactArgs(1),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadDNS()
			if err := d.Delete(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
)

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func init() {
	cmd.AddCommand(createCmd)
	cmd.AddCommand(deleteAllCmd)
	cmd.AddCommand(deleteCmd)
}

func loadDNS() dns.Module {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	r, err := dns.New(&c.DNS)
	if err != nil {
		panic(err)
	}
	return r
}
