package certtool

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/cert"
	"gitlab.com/slagit/vault/vaulttool/internal/arguments"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gopkg.in/yaml.v2"
)

type Configuration struct {
	Cert cert.Configuration
}

var (
	cmd = &cobra.Command{
		Use:   "cert",
		Short: "request certificates",
		Long:  `Request certificates`,
	}
	registerCmd = &cobra.Command{
		Use:               "register",
		Short:             "register a new certificate account",
		Long:              `Register a new account with a certificate provider`,
		Args:              cobra.ExactArgs(0),
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadCert()
			if err := d.Register(ctx); err != nil {
				panic(err)
			}
		},
	}
	requestCmd = &cobra.Command{
		Use:   "request <private-key> <output> <hostname>...",
		Short: "request a new certificate",
		Long:  `Request a new certificate`,
		Args:  cobra.MinimumNArgs(3),
		ValidArgsFunction: arguments.ArgList(
			arguments.PEMFile,
			arguments.PEMFile,
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			d := loadCert()

			eckey, err := cert.LoadSigner(args[0])
			if err != nil {
				panic(err)
			}
			chain, err := d.Request(ctx, eckey, args[2:]...)
			if err != nil {
				panic(err)
			}

			err = cert.SaveCertificates(args[1], chain)
			if err != nil {
				panic(err)
			}
		},
	}
)

func loadCert() cert.Module {
	f, err := config.Open()
	if err != nil {
		panic(err)
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &Configuration{}
	if err := d.Decode(c); err != nil {
		panic(err)
	}

	r, err := cert.New(&c.Cert)
	if err != nil {
		panic(err)
	}
	return r
}

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func init() {
	cmd.AddCommand(registerCmd)
	cmd.AddCommand(requestCmd)
}
