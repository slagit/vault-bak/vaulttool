package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/cmd/ansibletool"
	"gitlab.com/slagit/vault/vaulttool/cmd/certtool"
	"gitlab.com/slagit/vault/vaulttool/cmd/dnstool"
	"gitlab.com/slagit/vault/vaulttool/cmd/lbtool"
	"gitlab.com/slagit/vault/vaulttool/cmd/runtool"
	"gitlab.com/slagit/vault/vaulttool/cmd/sshtool"
	"gitlab.com/slagit/vault/vaulttool/cmd/usertool"
	"gitlab.com/slagit/vault/vaulttool/cmd/vaulttool"
	"gitlab.com/slagit/vault/vaulttool/cmd/vmtool"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
)

var (
	completionCmd = &cobra.Command{
		Use:   "completion [bash|zsh|fish|powershell]",
		Short: "Generate completion script",
		Long: `To load completions:

Bash:

$ source <(vaulttool completion bash)

# To load completions for each session, execute once:
Linux:
  $ vaulttool completion bash > /etc/bash_completion.d/vaulttool
MacOS:
  $ vaulttool completion bash > /usr/local/etc/bash_completion.d/vaulttool

Zsh:

# If shell completion is not already enabled in your environment you will need
# to enable it.  You can execute the following once:

$ echo "autoload -U compinit; compinit" >> ~/.zshrc

# To load completions for each session, execute once:
$ vaulttool completion zsh > "${fpath[1]}/_vaulttool"

# You will need to start a new shell for this setup to take effect.

Fish:

$ vaulttool completion fish | source

# To load completions for each session, execute once:
$ vaulttool completion fish > ~/.config/fish/completions/vaulttool.fish
`,
		DisableFlagsInUseLine: true,
		ValidArgs:             []string{"bash", "zsh", "fish", "powershell"},
		Args:                  cobra.ExactValidArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			switch args[0] {
			case "bash":
				cmd.Root().GenBashCompletion(os.Stdout)
			case "zsh":
				cmd.Root().GenZshCompletion(os.Stdout)
			case "fish":
				cmd.Root().GenFishCompletion(os.Stdout, true)
			case "powershell":
				cmd.Root().GenPowerShellCompletion(os.Stdout)
			}
		},
	}

	rootCmd = &cobra.Command{
		Use:   os.Args[0],
		Short: "SLAGIT.net Vault Cluster Manager",
		Long:  `A utility tool for managing vault clusters`,
	}

	stageCmd = &cobra.Command{
		Use:   "stage",
		Short: "Run individual tool stages",
		Long:  `Run individual stages commonly used as part of a top-level command`,
	}
)

func init() {
	rootCmd.AddCommand(completionCmd)
	rootCmd.PersistentFlags().String("config", "", "path to configuration file")
	config.AddPath("/etc")
	config.AddPath("$CONFIG")
	config.AddPath(".")
	config.BindPFlags(rootCmd.PersistentFlags().Lookup("config"))
	config.BindEnv("VAULTTOOL_CONFIG")
	config.SetConfigName("vaulttool.yaml")
	rootCmd.AddCommand(stageCmd)
	ansibletool.Mount(rootCmd)
	certtool.Mount(stageCmd)
	dnstool.Mount(stageCmd)
	lbtool.Mount(stageCmd)
	runtool.Mount(rootCmd)
	sshtool.Mount(rootCmd)
	usertool.Mount(rootCmd)
	vaulttool.Mount(rootCmd)
	vmtool.Mount(stageCmd)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
