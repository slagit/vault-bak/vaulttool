package ansibletool

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/slagit/vault/vaulttool/ansible"
	"gitlab.com/slagit/vault/vaulttool/internal/arguments"
	"gitlab.com/slagit/vault/vaulttool/internal/config"
	"gopkg.in/yaml.v2"
)

var (
	cmd = &cobra.Command{
		Use:   "ansible",
		Short: "ansible host secrets",
		Long:  `Manage vault secrets related to ansible hosts.`,
	}
	getPasswordCmd = &cobra.Command{
		Use:   "get-password <hostname>",
		Short: "get user password for host",
		Long:  `Get a user password for a host in the ansible storage area`,
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(
			validHostname,
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			a := loadAnsible()
			if err := a.GetPassword(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
	getRootCmd = &cobra.Command{
		Use:   "get-root <hostname>",
		Short: "get root password for host",
		Long:  `Get a root password for a host in the ansible storage area`,
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(
			validHostname,
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			a := loadAnsible()
			if err := a.GetRootPassword(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
	listHostsCmd = &cobra.Command{
		Use:               "list-hosts",
		Short:             "list hosts in the vault ansible storage area",
		Long:              `List all hosts currently in the ansible storage area`,
		Args:              cobra.NoArgs,
		ValidArgsFunction: arguments.ArgList(),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			a := loadAnsible()
			if err := a.ListHosts(ctx); err != nil {
				panic(err)
			}
		},
	}
	listKeysCmd = &cobra.Command{
		Use:   "list-keys <hostname>",
		Short: "list keys set for a specific hostname",
		Long:  `List all the keys set for a specific host`,
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(
			validHostname,
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			a := loadAnsible()
			if err := a.ListKeys(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
	setPasswordCmd = &cobra.Command{
		Use:   "set-password <hostname> <username>",
		Short: "set or change user password for host",
		Long:  `Setup or change a user password for a host in the ansible storage area`,
		Args:  cobra.ExactArgs(2),
		ValidArgsFunction: arguments.ArgList(
			validHostname,
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			a := loadAnsible()
			if err := a.SetPassword(ctx, args[0], args[1]); err != nil {
				panic(err)
			}
		},
	}
	setRootCmd = &cobra.Command{
		Use:   "set-root <hostname>",
		Short: "set or change root password for host",
		Long:  `Setup or change a root password for a host in the ansible storage area`,
		Args:  cobra.ExactArgs(1),
		ValidArgsFunction: arguments.ArgList(
			validHostname,
		),

		Run: func(cmd *cobra.Command, args []string) {
			ctx := context.Background()
			a := loadAnsible()
			if err := a.SetRootPassword(ctx, args[0]); err != nil {
				panic(err)
			}
		},
	}
	setValueCmd = &cobra.Command{
		Use:   "set-value <hostname> {self | shared-self | shared | recovery} <key> <value>",
		Short: "set a specific value in ansible storage area",
		Long:  `Set or change a specific value for a host in the ansible storage area`,
		Args:  cobra.ExactArgs(4),
		ValidArgsFunction: arguments.ArgList(
			validHostname,
			arguments.Choice([]string{
				string(ansible.PATH_SELF),
				string(ansible.PATH_SHARED_SELF),
				string(ansible.PATH_SHARED),
				string(ansible.PATH_RECOVERY),
			}),
		),

		Run: func(cmd *cobra.Command, args []string) {
			switch ansible.PathType(args[1]) {
			case ansible.PATH_SELF:
			case ansible.PATH_SHARED_SELF:
			case ansible.PATH_SHARED:
			case ansible.PATH_RECOVERY:
			default:
				panic(fmt.Errorf("Invalid path type: %s", args[1]))
			}

			ctx := context.Background()
			a := loadAnsible()
			if err := a.SetValue(ctx, args[0], ansible.PathType(args[1]), args[2], args[3]); err != nil {
				panic(err)
			}
		},
	}
)

func loadAnsible() ansible.Module {
	m, err := loadAnsibleSafe()
	if err != nil {
		panic(err)
	}
	return m
}

func loadAnsibleSafe() (ansible.Module, error) {
	f, err := config.Open()
	if err != nil {
		return nil, err
	}
	defer f.Close()

	d := yaml.NewDecoder(f)

	c := &ansible.Configuration{}
	if err := d.Decode(c); err != nil {
		return nil, err
	}

	r, err := ansible.New(c)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func Mount(parent *cobra.Command) {
	parent.AddCommand(cmd)
}

func init() {
	cmd.AddCommand(getPasswordCmd)
	cmd.AddCommand(getRootCmd)
	cmd.AddCommand(listHostsCmd)
	cmd.AddCommand(listKeysCmd)
	cmd.AddCommand(setPasswordCmd)
	cmd.AddCommand(setRootCmd)
	cmd.AddCommand(setValueCmd)
}

func validHostname(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	ctx := context.Background()
	a, err := loadAnsibleSafe()
	if err != nil {
		return nil, cobra.ShellCompDirectiveNoFileComp
	}
	return a.CompleteHostnames(ctx), cobra.ShellCompDirectiveNoFileComp
}
