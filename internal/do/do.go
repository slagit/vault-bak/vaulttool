package do

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"

	"github.com/digitalocean/godo"
	"gitlab.com/slagit/vault/vaulttool/internal/transport"
	"golang.org/x/oauth2"
	"gopkg.in/yaml.v2"
)

type Configuration struct {
	Endpoint  string
	Transport transport.Configuration `yaml:",inline"`
}

type Lister struct {
	Done        bool
	ListOptions godo.ListOptions
}

func (l *Lister) Update(resp *godo.Response) error {
	if resp.Links == nil || resp.Links.IsLastPage() {
		l.Done = true
		return nil
	}
	page, err := resp.Links.CurrentPage()
	if err != nil {
		return err
	}
	l.ListOptions.Page = page + 1
	return nil
}

func New(cfg *Configuration) (*godo.Client, error) {
	eps := cfg.Endpoint
	if eps == "" {
		eps = "https://api.digitalocean.com/"
	}

	ep, err := url.Parse(eps)
	if err != nil {
		return nil, err
	}

	at := getToken()
	if at == "" {
		return nil, errors.New("Please set the DIGITALOCEAN_ACCESS_TOKEN environment variable")
	}

	ts := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: at})

	trans, err := cfg.Transport.NewTransport("digital ocean")
	if err != nil {
		return nil, err
	}

	c := godo.NewClient(&http.Client{
		Transport: &oauth2.Transport{
			Base:   trans,
			Source: oauth2.ReuseTokenSource(nil, ts),
		},
	})
	c.BaseURL = ep

	return c, nil
}

func getToken() string {
	if at := os.Getenv("DIGITALOCEAN_ACCESS_TOKEN"); at != "" {
		return at
	}

	if homedir, err := os.UserHomeDir(); err == nil {
		if tok, err := ioutil.ReadFile(path.Join(homedir, ".config/doctl/config.yaml")); err == nil {
			var args struct {
				AuthContexts map[string]string `yaml:"auth-contexts"`
				Context      string            `yaml:"context"`
			}
			if err := yaml.Unmarshal(tok, &args); err == nil {
				return args.AuthContexts[args.Context]
			}
		}
	}

	return ""
}
