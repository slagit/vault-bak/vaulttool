package timeout

import (
	"fmt"
	"log"
	"time"
)

type Timeout struct {
	current int
	delay   time.Duration
	max     int
	name    string
	timeout bool
}

func (t *Timeout) Loop() bool {
	if t.current != 1 {
		time.Sleep(t.delay)
	}
	if t.current > t.max {
		return false
	}

	log.Printf("Waiting for %s (%d/%d)", t.name, t.current, t.max)
	t.current += 1
	return true
}

func (t *Timeout) Timeout() error {
	if t.timeout {
		return fmt.Errorf("Timeout occurred while %s", t.name)
	}
	return nil
}

func New(name string) *Timeout {
	return &Timeout{
		current: 1,
		delay:   10 * time.Second,
		max:     60,
		name:    name,
	}
}
