package config

import (
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/spf13/pflag"
)

var cp *ConfigPath

func init() {
	cp = New()
}

type ConfigPath struct {
	env      string
	filename string
	flag     *pflag.Flag
	paths    []string
}

func New() *ConfigPath {
	return &ConfigPath{}
}

func AddPath(p string) { cp.AddPath(p) }

func (cp *ConfigPath) AddPath(p string) {
	if p == "$CONFIG" || strings.HasPrefix(p, "$CONFIG"+string(os.PathSeparator)) {
		if d, err := os.UserConfigDir(); err != nil {
			return
		} else {
			p = d + p[7:]
		}
	}
	if filepath.IsAbs(p) {
		p = filepath.Clean(p)
	} else {
		if d, err := filepath.Abs(p); err != nil {
			return
		} else {
			p = d
		}
	}
	cp.paths = append(cp.paths, p)
}

func BindEnv(n string) { cp.BindEnv(n) }

func (cp *ConfigPath) BindEnv(n string) {
	cp.env = n
}

func BindPFlags(f *pflag.Flag) { cp.BindPFlags(f) }

func (cp *ConfigPath) BindPFlags(f *pflag.Flag) {
	cp.flag = f
}

func GetPath() (string, error) { return cp.GetPath() }

func (cp *ConfigPath) GetPath() (string, error) {
	if cp.flag != nil && cp.flag.Changed {
		return cp.flag.Value.String(), nil
	}

	if cp.env != "" {
		if envPath := os.Getenv(cp.env); envPath != "" {
			return envPath, nil
		}
	}

	if cp.filename != "" {
		for _, p := range cp.paths {
			configPath := path.Join(p, cp.filename)
			if _, err := os.Stat(configPath); err == nil {
				return configPath, nil
			}
		}
	}

	var b strings.Builder
	if cp.filename != "" && len(cp.paths) > 0 {
		fmt.Fprintf(&b, "%s not found in %s.", cp.filename, strings.Join(cp.paths, ", "))
	}
	if cp.env != "" {
		if cp.flag != nil {
			fmt.Fprintf(&b, " Please set %s or --%s=<filename>.", cp.env, cp.flag.Name)
		} else {
			fmt.Fprintf(&b, " Please set %s.", cp.env)
		}
	} else if cp.flag != nil {
		fmt.Fprintf(&b, " Please set --%s=<filename>.", cp.flag.Name)
	}
	if b.Len() == 0 {
		fmt.Fprintf(&b, "Invalid ConfigPath setup")
	}
	return "", errors.New(b.String())
}

func Open() (*os.File, error) { return cp.Open() }

func (cp *ConfigPath) Open() (*os.File, error) {
	path, err := cp.GetPath()
	if err != nil {
		return nil, err
	}

	return os.Open(path)
}

func SetConfigName(f string) { cp.SetConfigName(f) }

func (cp *ConfigPath) SetConfigName(f string) {
	cp.filename = f
}
