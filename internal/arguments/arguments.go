package arguments

import (
	"github.com/spf13/cobra"
)

type ValidArgsFunc func(*cobra.Command, []string, string) ([]string, cobra.ShellCompDirective)

func ArgList(fs ...ValidArgsFunc) ValidArgsFunc {
	return func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if len(args) < len(fs) {
			f := fs[len(args)]
			if f != nil {
				return f(cmd, args, toComplete)
			}
		}
		return nil, cobra.ShellCompDirectiveNoFileComp
	}
}

func Choice(opts []string) ValidArgsFunc {
	return func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return opts, cobra.ShellCompDirectiveNoFileComp
	}
}

func OneYamlFile(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	if len(args) == 0 {
		return YamlFile(cmd, args, toComplete)
	}
	return nil, cobra.ShellCompDirectiveNoFileComp
}

func Filename(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	return []string{"pem"}, cobra.ShellCompDirectiveDefault
}

func PEMFile(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	return []string{"pem"}, cobra.ShellCompDirectiveFilterFileExt
}

func YamlFile(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	return []string{"yaml", "json"}, cobra.ShellCompDirectiveFilterFileExt
}
