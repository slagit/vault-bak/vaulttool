package vault

import (
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"path"

	"github.com/hashicorp/vault/api"
	"gitlab.com/slagit/vault/vaulttool/internal/timeout"
	"gitlab.com/slagit/vault/vaulttool/internal/transport"
)

func NewClient(t *transport.Configuration, address string) (*api.Client, error) {
	httpclient, err := t.NewClient("vault")
	if err != nil {
		return nil, err
	}

	httpclient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}

	client, err := api.NewClient(&api.Config{
		Address:    address,
		HttpClient: httpclient,
	})
	if err != nil {
		return nil, err
	}

	return client, nil
}

func NewClientWithToken(t *transport.Configuration, address string) (*api.Client, error) {
	c, err := NewClient(t, address)
	if err != nil {
		return nil, err
	}

	if tok := os.Getenv("VAULT_TOKEN"); tok != "" {
		c.SetToken(tok)
		return c, nil
	}

	if homedir, err := os.UserHomeDir(); err == nil {
		if tok, err := ioutil.ReadFile(path.Join(homedir, ".vault-token")); err == nil {
			c.SetToken(string(tok))
			return c, nil
		}
	}

	return nil, errors.New("Please set the VAULT_TOKEN environment variable")
}

type RaftRemovePeerRequest struct {
	ServerID string `json:"server_id"`
}

func RaftRemovePeer(ctx context.Context, c *api.Client, opts *RaftRemovePeerRequest) error {
	r := c.NewRequest("POST", "/v1/sys/storage/raft/remove-peer")

	if err := r.SetJSONBody(opts); err != nil {
		return err
	}

	resp, err := c.RawRequestWithContext(ctx, r)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

func Wait(c *api.Client) error {
	t := timeout.New("vault to come online")
	for t.Loop() {
		_, err := c.Sys().Health()
		if err == nil {
			break
		}
	}
	return t.Timeout()
}
