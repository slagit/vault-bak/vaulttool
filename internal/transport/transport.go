package transport

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/hashicorp/go-cleanhttp"
)

type Configuration struct {
	CACertificateData string `yaml:"ca-certificate-data,omitempty"`
	CACertificate     string `yaml:"ca-certificate,omitempty"`
}

func (cfg *Configuration) NewTransport(name string) (*http.Transport, error) {
	certs, err := cfg.LoadCerts()
	if err != nil {
		return nil, err
	}

	cp, err := x509.SystemCertPool()
	if err != nil {
		return nil, err
	}

	if len(certs) != 0 {
		if ok := cp.AppendCertsFromPEM(certs); !ok {
			return nil, fmt.Errorf("Error loading %s CA certificate", name)
		}
	}

	t := cleanhttp.DefaultPooledTransport()
	t.TLSClientConfig = &tls.Config{
		MinVersion: tls.VersionTLS12,
		RootCAs:    cp,
	}

	return t, nil
}

func (cfg *Configuration) NewClient(name string) (*http.Client, error) {
	trans, err := cfg.NewTransport(name)
	if err != nil {
		return nil, err
	}

	return &http.Client{
		Transport: trans,
	}, nil
}

func (cfg *Configuration) LoadCerts() ([]byte, error) {
	if cfg.CACertificateData != "" {
		return []byte(cfg.CACertificateData), nil
	}

	if cfg.CACertificate != "" {
		return ioutil.ReadFile(cfg.CACertificate)
	}

	return nil, nil
}
