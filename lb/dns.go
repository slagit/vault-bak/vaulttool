package lb

import (
	"context"

	"gitlab.com/slagit/vault/vaulttool/dns"
)

type DNSConfiguration struct {
	Hostname string
	DNS      *dns.Configuration
}

type DNSModule struct {
	hostname string
	dns      dns.Module
}

func (m *DNSModule) Add(ctx context.Context, ip string) error {
	_, err := m.dns.Create(ctx, m.hostname, "A", ip)
	return err
}

func (m *DNSModule) Remove(ctx context.Context, ip string) error {
	return m.dns.DeleteAll(ctx, m.hostname, "A", ip)
}

func newDNS(cfg *DNSConfiguration) (Module, error) {
	if cfg == nil {
		cfg = &DNSConfiguration{}
	}

	var dnsModule dns.Module
	if cfg.DNS != nil {
		var err error
		dnsModule, err = dns.New(cfg.DNS)
		if err != nil {
			panic(err)
		}
	}

	return &DNSModule{
		hostname: cfg.Hostname,
		dns:      dnsModule,
	}, nil
}
