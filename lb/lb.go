package lb

import (
	"context"
	"fmt"
)

type Configuration struct {
	Type  string
	DNSLB *DNSConfiguration `yaml:"dns-lb"`
}

type Module interface {
	Add(context.Context, string) error
	Remove(context.Context, string) error
}

func New(config *Configuration) (Module, error) {
	switch config.Type {
	case "DNS":
		return newDNS(config.DNSLB)
	default:
		return nil, fmt.Errorf("Unknown load balancer type: %s", config.Type)
	}
}
