# SLAGIT.net Vault Controller

Scripts for managing a HA Vault cluster running on digital ocean.

## Development

### Testing Environment

Launch the test environment with docker compose:

```shell
docker-compose up
```

Enter the cli environment:

```shell
docker-compose exec cli /bin/sh
```

In the cli environment, add an example domain to the digital ocean simulator:

```shell
apk add curl gnupg libcap openssl vault
setcap cap_ipc_lock-ep /usr/sbin/vault
curl -H "Authorization: Bearer blah" -d '{"name": "example.com"}' -k https://fakedo:8443/v2/domains/
curl -H "Authorization: Bearer blah" -d '{"distribution": "CentOS", "name": "CentOS Example", "region": "fake1", "url": "http://example.com/image"}' -k https://fakedo:8443/v2/images/
curl -H "Authorization: Bearer blah" -d '{"name": "mykey", "public_key": "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBN2SfGwllFAe3moE8RqrbDp61dSClqhATHB/9f5TLzhrhLDjlPxkJCKDFIF53NEEF+rqpdlD84s2RqpIttqiqaI="}' -k https://fakedo:8443/v2/account/keys/
sh scripts/keygen.sh gpghome demo.asc
mkdir -p $HOME/.config/doctl
curl -k https://fakedo:8443/oob/doctl/config.yaml -o $HOME/.config/doctl/config.yaml
```

### DNS Testing

In the cli environment, test adding and removing dns entries:

```shell
go run ./cmd stage dns create a.example.com TXT hello --config=./examples/fakedo.yaml
go run ./cmd stage dns create b.example.com A 1.1.1.1 --config=./examples/fakedo.yaml
go run ./cmd stage dns delete 2.example.com --config=./examples/fakedo.yaml
```

### Certificate Testing

In the cli environment, test registering a new account and requesting a certificate:

```shell
openssl ecparam -genkey -name prime256v1 | openssl pkcs8 -nocrypt -topk8 > acme-key.pem
go run ./cmd stage cert register --config=./examples/fakedo.yaml
openssl ecparam -genkey -name prime256v1 | openssl pkcs8 -nocrypt -topk8 > example-key.pem
go run ./cmd stage cert request ./example-key.pem ./chain.pem example.com a.example.com --config=./examples/fakedo.yaml
```

### VM Testing

In the cli environment, test creating and removing a droplet:

```shell
go run ./cmd stage vm create MyVM /etc/passwd --config=./examples/fakedo.yaml
go run ./cmd stage vm delete 10.52.197.240 --config=./examples/fakedo.yaml
```

### Load Balancer Testing

```shell
go run ./cmd stage lb add 1.1.1.1 --config=./examples/fakedo.yaml
go run ./cmd stage lb remove 1.1.1.1 --config=./examples/fakedo.yaml
```

### Manager Testing

Setup the test environment:

```shell
sh scripts/vault-setup.sh
```

Create the bootstrap cluster:

```shell
sh scripts/vault-bootstrap.sh
```

Create the HA cluster:

```shell
sh scripts/vault.sh
```

(Optional) Show cluster status and members:

```sh
VAULT_ADDR=https://example.com VAULT_CACERT=/tmp/vault-demo/vault-ca.pem vault login -method=userpass username=demo password=password
VAULT_ADDR=https://example.com VAULT_CACERT=/tmp/vault-demo/vault-ca.pem vault status
VAULT_ADDR=https://example.com VAULT_CACERT=/tmp/vault-demo/vault-ca.pem vault operator raft list-peers
```

(Optional) Rotate a bootstrap node:

The <hostname> should be the name of the vault node to be replaced.

```shell
sh scripts/vault-bootstrap-rotate.sh <hostname>
```

(Optional) Rotate a HA node:

The <hostname> should be the name of the vault node to be replaced.

```shell
sh scripts/vault-bootstrap-rotate.sh <hostname>
```

(Optional) Test Run utility:

```sh
go run ./cmd run --config=./examples/test.yaml --role ansible env
```

(Optional) Test SSH utility:

```sh
/usr/sbin/sshd
VAULT_ADDR=https://example.com VAULT_CACERT=/tmp/vault-demo/vault-ca.pem vault login -method=userpass username=demo password=password
go run ./cmd ssh --config=./examples/test.yaml localhost
```
