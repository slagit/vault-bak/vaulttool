package vm

import (
	"context"
	"errors"
	"fmt"
	"log"

	"github.com/digitalocean/godo"
	"gitlab.com/slagit/vault/vaulttool/internal/do"
	"gitlab.com/slagit/vault/vaulttool/internal/timeout"
)

type DigitalOceanConfiguration struct {
	do.Configuration `yaml:",inline"`
	Region           string
	Size             string
	Image            string
	SSHKeys          []godo.DropletCreateSSHKey `yaml:"ssh-keys"`
	Tags             []string
	VPC              string
}

type DigitalOceanModule struct {
	client  *godo.Client
	region  string
	size    string
	image   string
	sshKeys []godo.DropletCreateSSHKey
	tags    []string
	vpc     string
}

func (m *DigitalOceanModule) Create(ctx context.Context, name, userData string) (string, error) {
	log.Printf("Looking for image %s in region %s", m.image, m.region)
	var image *godo.Image
Image:
	for l := new(do.Lister); !l.Done; {
		images, resp, err := m.client.Images.List(ctx, &l.ListOptions)
		if err != nil {
			return "", err
		}

		for i := range images {
			if images[i].Name == m.image {
				for _, regionName := range images[i].Regions {
					if regionName == m.region {
						image = &images[i]
						break Image
					}
				}
			}
		}

		err = l.Update(resp)
		if err != nil {
			return "", err
		}
	}
	if image == nil {
		return "", fmt.Errorf("Unable to locate image %s in region %s", m.image, m.region)
	}

	log.Printf("Looking for VPC %s in region %s", m.vpc, m.region)
	var vpc *godo.VPC
VPC:
	for l := new(do.Lister); !l.Done; {
		vpcs, resp, err := m.client.VPCs.List(ctx, &l.ListOptions)
		if err != nil {
			return "", err
		}

		for i := range vpcs {
			if vpcs[i].RegionSlug == m.region && vpcs[i].Name == m.vpc {
				vpc = vpcs[i]
				break VPC
			}
		}

		err = l.Update(resp)
		if err != nil {
			return "", err
		}
	}
	if vpc == nil {
		return "", fmt.Errorf("Unable to locate VPC %s in region %s", m.vpc, m.region)
	}

	log.Printf("Creating droplet %s...", name)
	droplet, _, err := m.client.Droplets.Create(ctx, &godo.DropletCreateRequest{
		Name:   name,
		Region: m.region,
		Size:   m.size,
		Image: godo.DropletCreateImage{
			ID: image.ID,
		},
		SSHKeys:  m.sshKeys,
		UserData: userData,
		Tags:     m.tags,
		VPCUUID:  vpc.ID,
	})
	if err != nil {
		return "", err
	}

	t := timeout.New("droplet to finish creating")
	for t.Loop() {
		droplet, _, err = m.client.Droplets.Get(ctx, droplet.ID)
		if err != nil {
			return "", err
		}
		if droplet.Status != "new" {
			break
		}
	}
	if err := t.Timeout(); err != nil {
		return "", err
	}

	ip, err := droplet.PublicIPv4()
	if err != nil {
		return "", err
	}

	return ip, nil
}

func (m *DigitalOceanModule) Delete(ctx context.Context, id string) error {
	log.Printf("Looking for droplet %s in region %s", id, m.region)

	for l := new(do.Lister); !l.Done; {
		droplets, resp, err := m.client.Droplets.List(ctx, &l.ListOptions)
		if err != nil {
			return err
		}

	Droplet:
		for i := range droplets {
			if droplets[i].Region.Slug != m.region {
				continue
			}

			ip, err := droplets[i].PublicIPv4()
			if err != nil {
				return err
			}

			if ip != id {
				continue
			}

		Tag:
			for _, tag := range m.tags {
				for _, t := range droplets[i].Tags {
					if t == tag {
						continue Tag
					}
				}
				continue Droplet
			}

			log.Printf("Deleting droplet %s\n", droplets[i].Name)
			_, err = m.client.Droplets.Delete(ctx, droplets[i].ID)
			return err
		}

		err = l.Update(resp)
		if err != nil {
			return err
		}
	}

	return errors.New("No matching droplet found.")
}

func newDigitalOcean(cfg *DigitalOceanConfiguration) (Module, error) {
	if cfg == nil {
		cfg = &DigitalOceanConfiguration{}
	}

	c, err := do.New(&cfg.Configuration)
	if err != nil {
		return nil, err
	}

	return &DigitalOceanModule{
		client:  c,
		region:  cfg.Region,
		size:    cfg.Size,
		image:   cfg.Image,
		sshKeys: cfg.SSHKeys,
		tags:    cfg.Tags,
		vpc:     cfg.VPC,
	}, nil
}
