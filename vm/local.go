package vm

import (
	"context"
	"encoding/binary"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"os"
	"os/exec"
	"path"
	"strconv"
	"time"

	"gitlab.com/slagit/vault/config"
)

var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func randomIP(cidr string) (string, error) {
	_, ipn, err := net.ParseCIDR(cidr)
	if err != nil {
		return "", err
	}

	ip := make([]byte, 4)
	binary.LittleEndian.PutUint32(ip, seededRand.Uint32())
	for i := range ip {
		ip[i] = (ipn.IP[i] & ipn.Mask[i]) | (ip[i] &^ ipn.Mask[i])
	}

	return net.IP(ip).String(), nil
}

type LocalVaultConfiguration struct {
	VaultExecutable string
	InstancePath    string
}

type LocalVaultModule struct {
	vaultExecutable string
	instancePath    string
}

func (m *LocalVaultModule) Create(ctx context.Context, name, userData string) (string, error) {
	ud, err := config.Parse([]byte(userData))
	if err != nil {
		return "", err
	}

	w := config.NewWriter(config.Filesystem())
	w.GroupName = "root"
	w.ManageHostname = false

	w.PrivateIP, err = randomIP("127.0.0.1/8")
	if err != nil {
		return "", err
	}

	w.PublicIP, err = randomIP("127.0.0.1/8")
	if err != nil {
		return "", err
	}

	w.ConfigRootPath = path.Join(m.instancePath, w.PublicIP)
	log.Printf("Generating vault configuration: %s/", w.ConfigRootPath)

	w.PIDPath = path.Join(w.ConfigRootPath, "pid")
	w.RaftPath = path.Join(w.ConfigRootPath, "raft")

	if err := os.MkdirAll(w.RaftPath, 0770); err != nil {
		return "", err
	}

	w.Write(name, ud)

	configPath := path.Join(w.ConfigRootPath, "config.hcl")
	go func() {
		log.Println("Scheduling vault server launch")
		time.Sleep(10 * time.Second)

		log.Println("Launching vault server")
		cmd := exec.Command(m.vaultExecutable, "server", "-config", configPath)
		if err := cmd.Start(); err != nil {
			log.Printf("Error launching vault server: %s", err)
		}
	}()

	return w.PublicIP, nil
}

func (m *LocalVaultModule) Delete(ctx context.Context, id string) error {
	instancePath := path.Join(m.instancePath, id)

	log.Println("Looking up PID for vault")
	pidPath := path.Join(instancePath, "pid")
	pidStr, err := ioutil.ReadFile(pidPath)
	if err != nil {
		return err
	}
	pid, err := strconv.Atoi(string(pidStr))
	if err != nil {
		return err
	}

	log.Printf("Killing pid: %d", pid)
	proc, err := os.FindProcess(pid)
	if err != nil {
		return err
	}
	if err := proc.Kill(); err != nil {
		log.Printf("Error while killing process: %s", err)
	}

	log.Printf("Removing vault working directory: %s", instancePath)
	if err := os.RemoveAll(instancePath); err != nil {
		return err
	}

	return nil
}

func newLocalVault(cfg *LocalVaultConfiguration) (Module, error) {
	if cfg == nil {
		cfg = &LocalVaultConfiguration{}
	}
	retval := &LocalVaultModule{
		vaultExecutable: cfg.VaultExecutable,
		instancePath:    cfg.InstancePath,
	}
	if retval.vaultExecutable == "" {
		retval.vaultExecutable = "/usr/sbin/vault"
	}
	if retval.instancePath == "" {
		retval.instancePath = "/tmp/vault"
	}
	return retval, nil
}
