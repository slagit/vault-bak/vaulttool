package vm

import (
	"context"
	"fmt"
)

type Configuration struct {
	Type         string
	DigitalOcean *DigitalOceanConfiguration `yaml:"digital-ocean"`
	LocalVault   *LocalVaultConfiguration   `yaml:"local-vault"`
}

type Module interface {
	Create(context.Context, string, string) (string, error)
	Delete(context.Context, string) error
}

func New(config *Configuration) (Module, error) {
	switch config.Type {
	case "DigitalOcean":
		return newDigitalOcean(config.DigitalOcean)
	case "LocalVault":
		return newLocalVault(config.LocalVault)
	default:
		return nil, fmt.Errorf("Unknown VM type: %s", config.Type)
	}
}
