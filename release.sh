#!/bin/sh

set -eu

VERSION=$1

handle() {
    mv "vaulttool-$1.zip" "vaulttool-$VERSION-$1.zip"
    sha256sum "vaulttool-$VERSION-$1.zip" > "vaulttool-$VERSION-$1.zip.sha256"
}

handle linux-amd64
