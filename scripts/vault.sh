#!/bin/sh

set -eu

. scripts/common.sh

export VAULT_ADDR=https://bootstrap.example.com

vault login \
    -method=userpass \
    username=demo \
    password=password

VAULT_DATA="$($SLAGIT_VAULT vault init --config=./examples/test.yaml --output=json)"

save_gpg_key 0 recover_1
save_gpg_key 1 recover_2
save_gpg_key 2 recover_3

# TODO: Wait for vault to become active
sleep 10

$SLAGIT_VAULT vault join --config=./examples/test.yaml
$SLAGIT_VAULT vault join --config=./examples/test.yaml

# TODO: Wait for vault to become active
sleep 10

vault token revoke -self

export VAULT_ADDR=https://example.com

vault login "$(get_vault_init_token)"

vault auth enable userpass
vault policy write operator - <<EOF
path "sys/storage/raft/configuration" {
  capabilities = [ "read" ]
}
path "sys/storage/raft/remove-peer" {
  capabilities = [ "update" ]
}
path "ssh-host-signer/config/ca" {
  capabilities = [ "read" ]
}
path "ssh-client-signer/sign/root" {
  capabilities = [ "update" ]
}
EOF
vault write auth/userpass/users/demo \
    password=password \
    policies=u-ansible,operator \
    token_max_ttl=30m

vault secrets enable -path ansible kv
vault policy write u-ansible - <<EOF
path "auth/token/create/ansible" {
  capabilities = [ "update" ]
}
EOF
vault policy write ansible - <<EOF
path "ansible/*" {
  capabilities = [ "create", "delete", "list", "read", "update" ]
}
EOF

vault write auth/token/roles/ansible allowed_policies=ansible

vault secrets enable -path=ssh-host-signer ssh
vault write ssh-host-signer/config/ca generate_signing_key=true
vault write ssh-host-signer/roles/host key_type=ca ttl=87600h allow_host_certificates=true allowed_domains="localhost" allow_bare_domains=true algorithm_signer=rsa-sha2-512
vault write -field=signed_key ssh-host-signer/sign/host cert_type=host public_key=@/etc/ssh/ssh_host_rsa_key.pub valid_principals=localhost > /etc/ssh/ssh_host_rsa_key-cert.pub

vault secrets enable -path=ssh-client-signer ssh
vault write ssh-client-signer/config/ca generate_signing_key=true
vault read -field=public_key ssh-client-signer/config/ca > /etc/ssh/trusted-user-ca-keys.pub
vault write ssh-client-signer/roles/root -<<"EOH"
{
  "allow_user_certificates": true,
  "algorithm_signer": "rsa-sha2-512",
  "allowed_users": "*",
  "allowed_extensions": "permit-pty,permit-port-forwarding",
  "default_extensions": [
    {
      "permit-pty": ""
    }
  ],
  "key_type": "ca",
  "default_user": "root",
  "ttl": "30m0s"
}
EOH

vault token revoke -self
