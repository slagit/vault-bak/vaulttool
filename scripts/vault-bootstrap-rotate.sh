#!/bin/sh

set -eu

. scripts/common.sh

export VAULT_ADDR=https://bootstrap.example.com

OLD_NODE="$1"

vault login \
    -method=userpass \
    username=demo \
    password=password

VAULT_DATA="$($SLAGIT_VAULT vault join --config=./examples/test-bootstrap.yaml --output=json)"
vault_unseal unseal_1
vault_unseal unseal_2

# TODO: Wait for vault to become active
sleep 10

$SLAGIT_VAULT vault remove "$OLD_NODE" --config=./examples/test-bootstrap.yaml

# TODO: Wait for vault to stabilize
sleep 10

vault token revoke -self
