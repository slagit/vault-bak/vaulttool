#!/bin/sh

export DIGITALOCEAN_ACCESS_TOKEN=blah
export DIGITALOCEAN_CACERT=examples/fakedo-ca.pem
export DIGITALOCEAN_ENDPOINT=https://fakedo:8443

export PEBBLE_CACERT=examples/pebble-ca.pem
export PEBBLE_ADMIN_ENDPOINT=https://pebble:15000

export TEST_PATH=/tmp/vault-demo

export VAULT_CACERT=$TEST_PATH/vault-ca.pem

export SLAGIT_VAULT="${SLAGIT_VAULT:-go run ./cmd}"

get_vault_fqdn() {
    jq -r .FQDN <<EOF
$VAULT_DATA
EOF
}

get_vault_addr() {
    printf "https://%s" "$(get_vault_fqdn)"
}

gpg_decrypt() {
    GNUPGHOME=$TEST_PATH/gpg_$1 gpg --decrypt
}

get_vault_init_token() {
    jq -r .RootToken.Key <<EOF | base64 -d | gpg_decrypt operator
$VAULT_DATA
EOF
}

get_vault_seal_key() {
    gpg_decrypt "$1" < "$TEST_PATH/$1.key"
}

save_gpg_key() {
    jq -r ".Keys[$1].Key" <<EOF | base64 -d > "$TEST_PATH/$2.key"
$VAULT_DATA
EOF
}

vault_unseal() {
    VAULT_ADDR="$(get_vault_addr)" vault operator unseal "$(get_vault_seal_key "$1")"
}
