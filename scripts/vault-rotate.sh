#!/bin/sh

set -eu

. scripts/common.sh

OLD_NODE="$1"

export VAULT_ADDR=https://bootstrap.example.com

vault login \
    -method=userpass \
    username=demo \
    password=password

VAULT_DATA="$($SLAGIT_VAULT vault join --config=./examples/test.yaml --output=json)"

# TODO: Wait for vault to become active
sleep 10

vault token revoke -self

export VAULT_ADDR=https://example.com

vault login \
    -method=userpass \
    username=demo \
    password=password

$SLAGIT_VAULT vault remove "$OLD_NODE" --config=./examples/test.yaml

# TODO: Wait for vault to stabilize
sleep 10

vault token revoke -self
