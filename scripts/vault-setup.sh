#!/bin/sh

set -eu

. scripts/common.sh

keygen() {
    mkdir -p "$1"

    GNUPGHOME=$1 gpg --batch --generate-key <<EOF
%no-protection
%transient-key
Key-Type: RSA
Key-Length: 1024
Key-Usage: encrypt
Name-Comment: $2
EOF

    GNUPGHOME=$1 gpg -a --export > "$1.asc"
}

apk add curl gnupg jq libcap openssh openssl vault
setcap cap_ipc_lock-ep /usr/sbin/vault

ssh-keygen -A
ssh-keygen -f /root/.ssh/id_rsa -N ""
passwd -u root
cat >> /etc/ssh/sshd_config <<EOF
HostKey /etc/ssh/ssh_host_rsa_key
HostCertificate /etc/ssh/ssh_host_rsa_key-cert.pub
TrustedUserCAKeys /etc/ssh/trusted-user-ca-keys.pub
EOF

curl \
    --cacert $DIGITALOCEAN_CACERT \
    --data '{"name": "example.com"}' \
    --fail \
    --header "Authorization: Bearer $DIGITALOCEAN_ACCESS_TOKEN" \
    --location \
    $DIGITALOCEAN_ENDPOINT/v2/domains/

keygen "$TEST_PATH/gpg_unseal_1" "Example Unseal 1"
keygen "$TEST_PATH/gpg_unseal_2" "Example Unseal 2"
keygen "$TEST_PATH/gpg_unseal_3" "Example Unseal 3"
keygen "$TEST_PATH/gpg_operator" "Example Operator"

openssl ecparam \
    -genkey \
    -name prime256v1 \
| openssl pkcs8 \
    -nocrypt \
    -topk8 > $TEST_PATH/acme-key.pem
$SLAGIT_VAULT stage cert register --config=./examples/test-bootstrap.yaml

curl \
    --cacert $PEBBLE_CACERT \
    --fail \
    --location \
    --output $VAULT_CACERT \
    $PEBBLE_ADMIN_ENDPOINT/roots/0
