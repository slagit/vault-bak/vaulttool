#!/bin/sh

set -eu

. scripts/common.sh

export VAULT_ADDR=https://bootstrap.example.com

VAULT_DATA="$($SLAGIT_VAULT vault init --config=./examples/test-bootstrap.yaml --output=json)"

save_gpg_key 0 unseal_1
save_gpg_key 1 unseal_2
save_gpg_key 2 unseal_3

vault_unseal unseal_1
vault_unseal unseal_2

# TODO: Wait for vault to become active
sleep 10

vault login "$(get_vault_init_token)"

vault auth enable userpass
vault secrets enable transit
vault policy write autounseal - <<EOF
path "transit/encrypt/autounseal" {
  capabilities = [ "update" ]
}
path "transit/decrypt/autounseal" {
  capabilities = [ "update" ]
}
EOF
vault policy write operator - <<EOF
path "auth/token/create/autounseal" {
  capabilities = [ "update" ]
  min_wrapping_ttl = "1s"
  max_wrapping_ttl = "2m"
}
path "sys/storage/raft/configuration" {
  capabilities = [ "read" ]
}
path "sys/storage/raft/remove-peer" {
  capabilities = [ "update" ]
}
path "transit/keys/autounseal/rotate" {
  capabilities = [ "update" ]
}
EOF
vault write auth/token/roles/autounseal \
    allowed_policies=autounseal \
    orphan=true
vault write auth/userpass/users/demo \
    password=password \
    policies=operator \
    token_max_ttl=30m
vault write -f transit/keys/autounseal
vault token revoke -self
