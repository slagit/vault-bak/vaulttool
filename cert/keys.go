package cert

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io/ioutil"
	"log"
	"os"
)

func LoadSigner(filename string) (crypto.Signer, error) {
	log.Printf("Loading private key from %s...", filename)
	r, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(r)
	r.Close()
	if err != nil {
		return nil, err
	}

	for {
		var block *pem.Block
		block, data = pem.Decode(data)
		if block == nil {
			return nil, errors.New("No suitable private key found")
		}
		if block.Type == "PRIVATE KEY" {
			if key, err := x509.ParsePKCS1PrivateKey(block.Bytes); err == nil {
				return key, nil
			}
			if key, err := x509.ParsePKCS8PrivateKey(block.Bytes); err == nil {
				switch key := key.(type) {
				case *rsa.PrivateKey, *ecdsa.PrivateKey:
					return key.(crypto.Signer), nil
				}
				log.Println("Skipping unknown key type in ACME private key")
			}
		}
	}
}

func SaveCertificates(filename string, chain []x509.Certificate) error {
	log.Printf("Saving certificate chain to %s...", filename)
	w, err := os.Create(filename)
	if err != nil {
		return err
	}

	for _, crt := range chain {
		err = pem.Encode(w, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: crt.Raw,
		})
		if err != nil {
			w.Close()
			return err
		}
	}

	return w.Close()
}
