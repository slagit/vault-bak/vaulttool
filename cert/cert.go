package cert

import (
	"context"
	"crypto"
	"crypto/x509"
	"fmt"
)

type Configuration struct {
	Type string
	ACME *ACMEConfiguration `yaml:"acme"`
}

type Module interface {
	Register(context.Context) error
	Request(context.Context, crypto.Signer, ...string) ([]x509.Certificate, error)
}

func New(config *Configuration) (Module, error) {
	switch config.Type {
	case "ACME":
		return newACME(config.ACME)
	default:
		return nil, fmt.Errorf("Unknown certificate type: %s", config.Type)
	}
}
