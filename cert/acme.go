package cert

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/x509"
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/slagit/vault/vaulttool/dns"
	"gitlab.com/slagit/vault/vaulttool/internal/transport"
	"golang.org/x/crypto/acme"
)

type ACMEConfiguration struct {
	Contact        []string
	Endpoint       string
	PrivateKeyFile string                  `yaml:"private-key-file"`
	Transport      transport.Configuration `yaml:",inline"`
	DNS            *dns.Configuration
}

type ACMEModule struct {
	client  acme.Client
	contact []string
	dns     dns.Module
}

func (m *ACMEModule) Register(ctx context.Context) error {
	if len(m.contact) == 0 {
		return errors.New("ACME contact configuration required for registration")
	}

	log.Println("Registering ACME account...")
	_, err := m.client.Register(ctx, &acme.Account{
		Contact: m.contact,
	}, acme.AcceptTOS)
	if err != nil {
		return err
	}
	return nil
}

func (m *ACMEModule) Request(ctx context.Context, signer crypto.Signer, hostnames ...string) ([]x509.Certificate, error) {
	o, err := m.verify(ctx, acme.DomainIDs(hostnames...))
	if err != nil {
		return nil, err
	}

	log.Println("Requesting certificate from ACME server...")
	csr, err := x509.CreateCertificateRequest(rand.Reader, &x509.CertificateRequest{
		DNSNames: hostnames,
	}, signer)
	if err != nil {
		return nil, err
	}

	der, _, err := m.client.CreateOrderCert(ctx, o.FinalizeURL, csr, true)
	if err != nil {
		return nil, err
	}

	chain := make([]x509.Certificate, len(der))
	for i := range der {
		crt, err := x509.ParseCertificate(der[i])
		if err != nil {
			return nil, err
		}
		chain[i] = *crt
	}

	return chain, nil
}

func (m *ACMEModule) deactivatePendingAuthz(uri []string) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()
	for _, u := range uri {
		z, err := m.client.GetAuthorization(ctx, u)
		if err == nil && z.Status == acme.StatusPending {
			log.Println("Revoking authorization request on ACME server...")
			m.client.RevokeAuthorization(ctx, u)
		}
	}
}

func (m *ACMEModule) fulfill(ctx context.Context, chal *acme.Challenge, domain string) (cleanup func(), err error) {
	switch chal.Type {
	case "dns-01":
		rec, err := m.client.DNS01ChallengeRecord(chal.Token)
		if err != nil {
			return nil, err
		}
		hostname := "_acme-challenge." + domain
		key, err := m.dns.Create(ctx, hostname, "TXT", rec)
		if err != nil {
			return nil, err
		}
		return func() {
			err := m.dns.Delete(ctx, key)
			if err != nil {
				log.Printf("ERROR occurred while removing TXT dns entry for %s: %s", hostname, err)
			}
		}, nil
	}
	return nil, fmt.Errorf("acme: unknown challenge type %q", chal.Type)
}

func (m *ACMEModule) verify(ctx context.Context, id []acme.AuthzID) (*acme.Order, error) {
AuthorizeOrderLoop:
	for {
		log.Println("Creating a new order on ACME server...")
		o, err := m.client.AuthorizeOrder(ctx, id)
		if err != nil {
			return nil, err
		}
		defer func(urls []string) {
			go m.deactivatePendingAuthz(urls)
		}(o.AuthzURLs)

		switch o.Status {
		case acme.StatusReady:
			return o, nil
		case acme.StatusPending:
			// Continue normal Order-based flow.
		default:
			return nil, fmt.Errorf("acme: invalid new order status %q; order URL: %q", o.Status, o.URI)
		}

		for _, zurl := range o.AuthzURLs {
			log.Println("Requesting challenges from ACME server...")
			z, err := m.client.GetAuthorization(ctx, zurl)
			if err != nil {
				return nil, err
			}
			if z.Status != acme.StatusPending {
				continue
			}
			var chal *acme.Challenge
			for _, c := range z.Challenges {
				if m.dns != nil && c.Type == "dns-01" {
					chal = c
					break
				}
			}
			if chal == nil {
				return nil, fmt.Errorf("acme: unable to satisfy %q for domain %q: no viable challenge type found", z.URI, z.Identifier.Value)
			}
			cleanup, err := m.fulfill(ctx, chal, z.Identifier.Value)
			if err != nil {
				continue AuthorizeOrderLoop
			}
			defer cleanup()
			log.Println("Requesting ACME server verify challenge...")
			if _, err := m.client.Accept(ctx, chal); err != nil {
				continue AuthorizeOrderLoop
			}
			log.Println("Waiting for ACME server to verify challenge...")
			if _, err := m.client.WaitAuthorization(ctx, z.URI); err != nil {
				continue AuthorizeOrderLoop
			}
		}

		log.Println("Waiting for ACME server to complete order...")
		o, err = m.client.WaitOrder(ctx, o.URI)
		if err != nil {
			continue AuthorizeOrderLoop
		}
		return o, nil
	}
}

func newACME(cfg *ACMEConfiguration) (Module, error) {
	if cfg == nil {
		cfg = &ACMEConfiguration{}
	}

	if cfg.PrivateKeyFile == "" {
		return nil, errors.New("cert.privatey-key-file is required")
	}
	signer, err := LoadSigner(cfg.PrivateKeyFile)
	if err != nil {
		return nil, err
	}

	var dnsModule dns.Module
	if cfg.DNS != nil {
		dnsModule, err = dns.New(cfg.DNS)
		if err != nil {
			panic(err)
		}
	}

	client, err := cfg.Transport.NewClient("acme")
	if err != nil {
		return nil, err
	}

	return &ACMEModule{
		client: acme.Client{
			DirectoryURL: cfg.Endpoint,
			HTTPClient:   client,
			Key:          signer,
		},
		contact: cfg.Contact,
		dns:     dnsModule,
	}, nil
}
